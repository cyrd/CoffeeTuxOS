![](images/CofeeTuxOS_welcome.PNG)

The documentation can be found in the main folder ( CoffeeTux___dokumentacja.pdf ).

CoffeeTux simulates many of the algorithms and data structures used in most
of the Linux operating system distributions and emulates the CPU.
You will find the round-robin, I-nodes, and registers of the processor inside
our project.

The system is built of the following momdules written by 6 people:
- CPU
- Process management
- Shell
- Filesystem
- External device (IO)
- RAM memory

Type 'help' in the command line to get more info about available commands.

To share ideas and thoughts, feel free to drop email: michal.werda1@gmail.com.

![](images/CoffeeTux_small.jpg)