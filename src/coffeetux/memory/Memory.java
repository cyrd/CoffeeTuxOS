/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffeetux.memory;

import coffeetux.Process_Management.*;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
//import java.util.List;
import java.util.LinkedList;
import coffeetux.filesystem.*;
import Console.Window;
import coffeetux.filesystem.FileSystem;


/**
 *
 * @author Piotr
 */
public class Memory
{

    PhysicalMemory memory;
    LinkedList<Integer> free_frames;
    LinkedList<LRU> LRU_stack;
    //  LinkedList<DummyProcess> process_list = new LinkedList();
    int[] swap_adress;
    static String swap_file;
    static int swap_size;
    LinkedList<Integer> swap_free_blocks;
    public Window console;
    public Process_Management processes;
    /* public Memory(Window console)
     {
     this.console = console;
     }*/

    public Memory()
    {
        console = new Window("Memory log", 400, 300);
        memory = new PhysicalMemory();
        free_frames = new LinkedList();
        LRU_stack = new LinkedList();
        //  LinkedList<DummyProcess> process_list = new LinkedList();
        swap_adress = new int[16];
        swap_file = new String("Swap.dat");
        swap_size = 0;
        swap_free_blocks = new LinkedList();
        processes = null;
        for (int i = 0; i < 16; i++)
        {
            free_frames.add(i);
            swap_adress[i] = (-1);
        }
        try
        {
            RandomAccessFile save = new RandomAccessFile(swap_file, "rw");
            save.setLength(0);

        } catch (IOException e)
        {
            //e.printStackTrace(console);
        }

    }
    private int RemovePage() throws IOException, ClassNotFoundException
    {
        PrintLRU();
        LRU tmp = LRU_stack.pollLast();
        Page_tab TMP = processes.getPage(tmp.PID, tmp.page_number);
        free_frames.add(TMP.adress);
        RandomAccessFile save = null;
        console.println("Victim -> Page " + tmp.page_number + " sent to swap, PID: " + tmp.PID);
        PrintPage(processes.getPage(tmp.PID, tmp.page_number));
        console.println("Released frame: " + TMP.adress);

        try
        {
            save = new RandomAccessFile(swap_file, "rw");
            int i = TMP.adress * 32;
            TMP.valid = false;
            TMP.exist = true;
            if (swap_adress[TMP.adress] < 0)
            {
                if (swap_free_blocks.isEmpty())
                {
                    save.seek(swap_size);
                } else
                {
                    save.seek(swap_free_blocks.pollFirst());
                }

                TMP.adress = (int) save.getFilePointer();

            } else
            {
                save.seek(swap_adress[TMP.adress]);
                TMP.adress = swap_adress[TMP.adress];
            }
            int j = i + 32;
            int x = 0;
            for (; i < j; i++)
            {
                save.writeChar(memory.ReadByte(i));
                //memory.WriteByte(i, 20);
                x++;
            }
            if (swap_size < (int) save.getFilePointer())
            {
                swap_size = (int) save.getFilePointer();
            }

        } catch (IOException e)
        {
            console.println("SWAP error" + "");
            e.printStackTrace(System.out);
            System.in.read();
        } finally
        {
            if (save != null)
            {
                save.close();
            }

        }
        return 0;
    }

    void PrintPage(Page_tab page)
    {
        int start = page.adress * 32;
        console.println("Printing page:" + "");
        for (int i = start; i < start + 32; i++)
        {
            console.print(memory.ReadByte(i) + " ");
        }
        console.println("");
    }

    private Integer GetSwapPage(int log_adress, ArrayList<Page_tab> page_table, int PID) throws IOException
    {
        RandomAccessFile save = null;
        Page_tab tmp = page_table.get(log_adress);
        try
        {
            save = new RandomAccessFile(swap_file, "r");
            save.seek(tmp.adress);

            if (free_frames.isEmpty())
            {
                RemovePage();
            }
            int frame = free_frames.pop();
            //ArrayList<Page> TMP = Findpage_table(PID);
            tmp.valid = true;
            swap_adress[frame] = tmp.adress;
            tmp.adress = frame;
            int start = frame * 32;

            for (int i = 0; i < 32; i++)
            {
                memory.WriteByte(start + i, save.readChar());
            }
            console.println("Read from swap: " + swap_adress[frame] + ", page: " + log_adress + " PID: " + PID + "to frame" + frame);
            PrintPage(page_table.get(log_adress));
        } catch (IOException x)
        {
            // console.println(x.toString());
        } catch (ClassNotFoundException e)
        {
            // console.println(e.toString());
        } finally
        {
            if (save != null)
            {
                save.close();
            }
            ;
        }
        return 0;
    }

    public void SaveProcess(int PID, ArrayList<Page_tab> page_table, FileInfo fi)//PID, pagetable, fileinfo
    {
        try
        {
            console.println("Writing complete process pages to disk, PID: " + PID);
            //  FileSystem.Truncate(fi, 0);
            RandomAccessFile save = null;
            save = new RandomAccessFile(swap_file, "r");
            for (int i = 0; i < page_table.size(); ++i)
            {
                char tmp[] = new char[32];
                Page_tab TMP = page_table.get(i);
                String tmp_data = new String();
                if (TMP.exist == false && TMP.valid == false)
                {
                    continue;
                } else if (TMP.valid == false)
                {

                    save.seek(TMP.adress);
                    if (TMP.adress >= 0 || swap_adress[TMP.adress] >= 0)
                    {
                        swap_free_blocks.add(TMP.adress);
                    }
                    for (int j = 0; j < 32; ++j)
                    {
                        tmp[j] = save.readChar();
                        //   tmp_data = tmp_data + tmp[j];
                    }
                } else
                {
                    free_frames.add(TMP.adress);
                    if (swap_adress[TMP.adress] >= 0)
                    {
                        swap_free_blocks.add(swap_adress[TMP.adress]);
                    }
                    swap_adress[TMP.adress] = -1;
                    LRUDelete(PID, i);
                    for (int j = 0; j < 32; ++j)
                    {

                        tmp[j] = memory.ReadByte(Recalculate(j, TMP));
                        tmp_data = tmp_data + tmp[j];
                    }
                }
                console.println("Write to disk-> pid: " + fi.pid + " page: " + TMP.page_id + " \n" + tmp_data);
                try
                {
                    //WriteToDisk(tmp, PID, fileinfo, TMP.page_number*32);
                    FileSystem.Write(fi, tmp_data, i * 32);
                }
                catch (FileSystemException ex)
                {
                }

            }
        } catch (IOException e)
        {

        }
    }

    private int GetDiskPage(int log_adress, ArrayList<Page_tab> page_table, FileInfo fi, int PID)
    {
        if (free_frames.isEmpty())
        {
            try
            {
                RemovePage();
            } catch (IOException e)
            {
                e.printStackTrace(System.out);
            } catch (ClassNotFoundException x)
            {
                x.printStackTrace(System.out);
            }
        }

        page_table.get(log_adress).adress = free_frames.pop();
        Page_tab lol = page_table.get(log_adress);//.exist=true;
        //process_list.get(PID).page_table.get(log_adress).valid=true;
        lol.exist = true;
        lol.valid = true;
        swap_adress[lol.adress] = -1;
        String tmp = "";
        try
        {
            tmp = FileSystem.Read(fi, 32, log_adress * 32);
        }
        catch (FileSystemException ex)
        {
        }
        int z = 0;
        for (int j = lol.adress * 32; j < ((lol.adress * 32) + 32); j++)
        {

            memory.WriteByte((lol.adress * 32) + z, tmp.charAt(z));
            z++;
        }
        console.println("Read from disk-> page: " + log_adress + " PID: " + PID + " to frame " + lol.adress);
        PrintPage(lol);
        return 0;

    }

    private int Recalculate(int offset, Page_tab tmp)//PID, adress,offset
    {

        int adress = 0;
        adress = (tmp.adress * 32) + offset;
        // console.println("przeliczono adres : " + adress + "");
        return adress;

    }

    public int MemoryWrite(int PID, char data, int log_adress, int offset, ArrayList<Page_tab> page_table, FileInfo fi)
    {

        if (!CheckPage(log_adress, page_table, fi, PID))
        {
          if(free_frames.isEmpty())
          {
            try
            {
                RemovePage();
            }
            catch(Exception e)
            {
            }
          }
          console.println("MemoryWrite -> demanded page does not exist. Creating.");
          int frame = free_frames.pop();
          int tmp =page_table.size()-1;
          page_table.add(new Page_tab(tmp,frame,true,false));
          console.println("Created page: " + tmp +" PID: " + PID +" alocated in frame " + frame);
          swap_adress[frame]=-1;
                
        }
        int phys_adress =0;
        try
        {
           phys_adress = Recalculate(offset, page_table.get(log_adress));
        }
        catch(IndexOutOfBoundsException e)
        {
            MemoryWrite(PID,data,log_adress,offset,page_table, fi);
        }
        
        LRU(PID, log_adress);
        // console.println("Writng: " + data + "");
        //FileSystem.Truncate(fi);
        return memory.WriteByte(phys_adress, data);

        //return -1;
    }
    private boolean CheckPage(int log_adress, ArrayList<Page_tab> page_table, FileInfo fi, int PID)

    {
        if (log_adress >= page_table.size())
        {
            return false;
        }
        Page_tab tmp = page_table.get(log_adress);
        if ((tmp.valid == false) && (tmp.exist))
        {
            try
            {
                GetSwapPage(log_adress, page_table, PID);
                return true;
            } catch (IOException e)
            {
                e.printStackTrace(System.out);
            }
        } else if ((tmp.valid == false)
                && (tmp.exist == false))
        {
            GetDiskPage(log_adress, page_table, fi, PID);
            return true;
        }
        return true;
    }

    public char MemoryRead(int PID, int log_adress, int offset, ArrayList<Page_tab> page_table, FileInfo fi)
    {
        CheckPage(log_adress, page_table, fi, PID);
        LRU(PID, log_adress);
        
        char tmp = '#';
        try{
            tmp = memory.ReadByte(Recalculate(offset, page_table.get(log_adress)));
        }
        catch(Exception e)
        {
            console.println("Error! Incorrect memory reference. Terminating program.");
            return '#';
        }
         
        //console.println("Reading: " + tmp + "");
        return tmp;
        // return -1;
    }

    private void LRUDelete(int PID, int log)
    {
        // LRU tmp = new LRU(PID, page_number);
        for (int i = 0; i < LRU_stack.size(); i++)
        {
            // console.printlnf("lru %d : %d, %d ", i,LRU_stack.get(i).PID, LRU_stack.get(i).page_number);
            if (LRU_stack.get(i).PID == PID && LRU_stack.get(i).page_number == log)
            {
                LRU_stack.remove(i);
            }

        }
    }

    private void LRU(int PID, int log)
    {
        // LRU tmp = new LRU(PID, page_number);
        for (int i = 0; i < LRU_stack.size(); i++)
        {
            // console.printlnf("lru %d : %d, %d ", i,LRU_stack.get(i).PID, LRU_stack.get(i).page_number);
            if (LRU_stack.get(i).PID == PID && LRU_stack.get(i).page_number == log)
            {
                LRU_stack.remove(i);
            }

        }
        LRU_stack.addFirst(new LRU(PID, log));

        /*   try
         {
         System.in.read();
         } catch (IOException e)
         {
         };*/
    }

    public void PrintLRU()
    {
        console.println("LRU STACK -> "+ "size: " +LRU_stack.size() + " \nPID:\tPage:");

        for (int i = 0; i < LRU_stack.size(); i++)
        {
            console.println(LRU_stack.get(i).PID + "\t" + LRU_stack.get(i).page_number);
        }
        console.println("");
    }

    public void PrintShellLRU()
    {
        if (LRU_stack.size() > 0)
        {

            System.out.println("LRU STACK: \nPID:\tPage:");

            for (int i = 0; i < LRU_stack.size(); i++)
            {
                System.out.println(LRU_stack.get(i).PID + "\t" + LRU_stack.get(i).page_number);
            }
            System.out.println("");
        } else
        {
            System.out.println("LRU stack is empty");
        }
    }

    public void PrintFreeFrames()
    {
        System.out.println(free_frames);
       // System.out.print(free_frames.);
    }

    public void PrintFrame(int frame_number)
    {
        if (frame_number > 15)
        {
            System.out.print("Error: frame does not exist!");
        }
        int adress = frame_number * 32;
        for (int i = adress; i < adress + 32; i++)
        {
            System.out.print(memory.ReadByte(i));
        }
        System.out.print("\n");
    }
    
    public void MemoryStatus()
    {
        LinkedList<Integer> x = new LinkedList();
        for(int i =0; i<LRU_stack.size() ;i++)
        {
          x.add( processes.getPage(LRU_stack.get(i).PID,LRU_stack.get(i).page_number).adress);
        }
       // Collections.sort(x);
        System.out.println("Memory status:");
        System.out.print("Frame\tPID\tPage\n");
        for(int i =0; i<16 ;i++)
        {
            if(x.indexOf(i)>=0)
            {
                System.out.println( i + "\t"+LRU_stack.get(x.indexOf(i)).PID + "\t"+ LRU_stack.get(x.indexOf(i)).page_number );
            }
            else
                System.out.println( i + "\t"+"F   R   E   E" );
          
        }
    }

    public void Shutdown()
    {
        console.dispose();
    }

}
