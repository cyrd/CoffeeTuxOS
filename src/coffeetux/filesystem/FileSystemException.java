package coffeetux.filesystem;

public class FileSystemException extends Exception
{

    private int errorCode;
    private static String msgs[] =
    {
        "File not found.", "Invalid path.", "Access denied.", "Can't change directory to file.", "File handler is null.", "Flag READ is not set.", "Flag WRITE is not set.", "Can't create file because of nodes limit."
    };

    public FileSystemException(int error)
    {
        super(msgs[error]);
        errorCode = error;
    }

    public int errorCode()
    {
        return errorCode;
    }

    public String toString()
    {
        return getMessage();
    }
}
