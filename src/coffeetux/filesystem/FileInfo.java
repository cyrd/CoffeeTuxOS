package coffeetux.filesystem;

import java.util.logging.Level;
import java.util.logging.Logger;

public class FileInfo
{

    public int fileID;
    public int filePtr;
    String dirlist;
    public int pid;
    boolean removable = false;
    private boolean removed = false;

    public FileInfo(int fid, int fptr, String dl, int pid)
    {
        fileID = fid;
        filePtr = fptr;
        dirlist = dl;
        this.pid = pid;
    }

    public FileInfo()
    {
        fileID = -1;
        dirlist = null;
    }

    public FileInfo(FileInfo fi)
    {
        fileID = fi.fileID;
        filePtr = fi.filePtr;
        dirlist = fi.dirlist;
        pid = fi.pid;
    }

    public void Close()
    {
        if (removable && !removed)
        {
            removed = true;
            FileSystem.removeFileFromList(fileID);
        }
    }

    @Override
    protected void finalize()
    {
        Close();

        try
        {
            super.finalize();
        }
        catch (Throwable ex)
        {
            Logger.getLogger(FileInfo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
