package coffeetux.filesystem;

import Console.Window;
import coffeetux.Process_Management.Process_Management;
import static coffeetux.filesystem.Converter.ConvertData;
import static coffeetux.filesystem.Converter.RecoverData;
import coffeetux.io.devicecollector;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeMap;

class CompareListEntries implements Comparator<String[]>
{

    @Override
    public int compare(String[] a, String[] b)
    {
        if (a[1].compareTo(b[1]) == 0)
        {
            return a[0].compareTo(b[0]);
        }
        return a[1].compareTo(b[1]);
    }
}

public class FileSystem
{

    static FileDisk dysk;
    static TreeMap<String, GlobalFileInfo> pliki;
    static Window Log;
    public static Process_Management pm = null;
    public static devicecollector dc = null;
    public static final int F_READ = 2;
    public static final int F_WRITE = 4;
    public static final int F_APPEND = 8;
    public static final int F_ERASE = 16;
    private final static String[][] zamiany_perms
                                    =
            {
                {
                    "0", "---"
                },
                {
                    "1", "--x"
                },
                {
                    "2", "-w-"
                },
                {
                    "3", "-wx"
                },
                {
                    "4", "r--"
                },
                {
                    "5", "r-x"
                },
                {
                    "6", "rw-"
                },
                {
                    "7", "rwx"
                }
            };
    private final static String[] typy_plikow =
    {
        "EMPTY", "DIR", "FILE", "EXE", "TEP"
    };

    /*  public static void main(String[] args)
     {
     //    new FileSystem();
     }*/
    public static void SetFileType(FileInfo fi, int type) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        ValidateAccess(fi.fileID, fi.pid, F_WRITE);
        dysk.setFileInfo(fi.fileID, type, -1, -1, -1);
    }

    public static void shutdown()
    {
        dysk.shutdown();
        Log.dispose();
    }

    /*public static int RecognizeFileType(String abrv)
     {
     abrv = abrv.toUpperCase();
     for (int i = 0; i < typy_plikow.length; i++)
     {
     if (abrv.equals(typy_plikow[i]))
     {
     return i;
     }
     }
     return -1;
     }*/
    public static short GetFileType(FileInfo fi) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        ValidateAccess(fi.fileID, fi.pid, F_READ);
        return (short) Math.abs(((dysk.getFileInfo(fi.fileID) & -8192) >> 13));
    }

    public static short IsFileDir(FileInfo fi) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        ValidateAccess(fi.fileID, fi.pid, F_READ);
        if (((dysk.getFileInfo(fi.fileID) & 512) >> 9) == 1)
        {
            return 1; //dir
        }
        else
        {
            return 0;
        }
    }

    public static short GetFileSize(FileInfo fi) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        ValidateAccess(fi.fileID, fi.pid, F_READ);
        return dysk.getFileSize(fi.fileID);
    }

    public static short GetFilePermissions(FileInfo fi) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        ValidateAccess(fi.fileID, fi.pid, F_READ);
        return (short) (dysk.getFileInfo(fi.fileID) & 511);
    }

    public static void SetFilePermissions(FileInfo fi, int perms) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        ValidateAccess(fi.fileID, fi.pid, F_WRITE);
        dysk.setFileInfo(fi.fileID, -1, -1, -1, perms);
    }

    public static byte GetFileUID(FileInfo fi) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        ValidateAccess(fi.fileID, fi.pid, F_READ);
        return dysk.getFileUID(fi.fileID);
    }

    public static void SetFileUID(FileInfo fi, int uid) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        ValidateAccess(fi.fileID, fi.pid, F_WRITE);
        dysk.setFileUID(fi.fileID, uid);
    }

    public static byte GetFileGID(FileInfo fi) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        ValidateAccess(fi.fileID, fi.pid, F_READ);
        return dysk.getFileGID(fi.fileID);
    }

    public static void SetFileGID(FileInfo fi, int gid) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        ValidateAccess(fi.fileID, fi.pid, F_WRITE);
        dysk.setFileGID(fi.fileID, gid);
    }

    public static String ResolvePath(FileInfo fi) throws FileSystemException
    {
        String path = "/";
        int i;
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        String temp[] = fi.dirlist.split("/");
        for (i = 1; i < temp.length; i++)
        {
            path += ResolveName(Integer.parseInt(temp[i]), Integer.parseInt(temp[i - 1]), fi.pid) + "/";
        }
        if ((dysk.getFileInfo(Integer.parseInt(temp[i - 1])) & 512) >> 9 == 0)
        {
            path = path.replaceAll("/$", "");
        }
        return path;
    }

    public static String ResolveName(FileInfo file, FileInfo dir) throws FileSystemException
    {
        if (file == null || dir == null)
        {
            throw new FileSystemException(4);
        }
        return ResolveName(file.fileID, dir.fileID, dir.pid);
    }

    private static String ResolveName(int file_id, int dir_id, int pid) throws FileSystemException
    {
        ValidateAccess(dir_id, pid, F_READ);
        Log.print("Wczytywanie nazwy pliku (ID: " + file_id + ")...");
        String nazwa = "";
        if (file_id == 0)
        {
            return "";
        }
        FileInfo tmp = new FileInfo(dir_id, 0, "", 0);
        DirEnt de = new DirEnt();
        for (int i = 0; i < dysk.getFileSize(dir_id);)
        {
            Read(tmp, de, i);
            if (de.nodeID == file_id)
            {
                nazwa = de.name;
                break;
            }
            i += de.name.length() + 2;
        }
        return nazwa;
    }

    public static ArrayList<String[]> ListDir(FileInfo fi) throws FileSystemException
    {
        ArrayList<String[]> out = new ArrayList();
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        Log.println("Wczytywanie zawartosci katalogu (ID: " + fi.fileID + ")...");
        DirEnt de = new DirEnt();
        ValidateAccess(fi.fileID, fi.pid, F_READ);
        for (int i = 0; i < dysk.getFileSize(fi.fileID);)
        {
            Read(fi, de, i);
            String info[] = new String[4];
            info[0] = de.name;
            try
            {
                info[1] = typy_plikow[Math.abs((dysk.getFileInfo(de.nodeID) & -8192) >> 13)];
            }
            catch (IndexOutOfBoundsException e)
            {
                info[1] = "UNKNOWN";
            }
            if (!info[1].equals(typy_plikow[0]))
            {
                info[2] = String.format("%03o", dysk.getFileInfo(de.nodeID) & 511);
                for (int j = 0; j < 8; j++)
                {
                    info[2] = info[2].replaceAll(zamiany_perms[j][0], zamiany_perms[j][1]);
                }
                info[3] = dysk.getFileSize(de.nodeID) + " B";
                i += de.name.length() + 2;
                out.add(info);
            }
            else
            {
                FileInfo file = new FileInfo(de.nodeID, 0, "", 0);
                FileInfo dir = new FileInfo(fi.fileID, 0, "", 0);
                Unlink(file, dir);
            }
        }
        out.sort(new CompareListEntries());
        return out;
    }

    public static String[] FileInfoToPrint(String path, int pid) throws FileSystemException
    {
        FileInfo fi = Open(path, F_READ, pid);
        fi.removable = false;
        return FileInfoToPrint(fi);
    }

    public static String[] FileInfoToPrint(FileInfo fi) throws FileSystemException
    {
        int i = 0;
        String info[] = new String[6];
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        try
        {
            info[0] = "File type: " + typy_plikow[GetFileType(fi)];
        }
        catch (IndexOutOfBoundsException e)
        {
            info[0] = "UNKNOWN";
        }
        info[1] = "File size: " + Short.toString(GetFileSize(fi)) + " B";
        info[2] = "Permissions: " + Integer.toString(GetFilePermissions(fi), 8);
        info[3] = "UID: " + Short.toString(GetFileUID(fi)) + " " + "GID: " + Short.toString(GetFileGID(fi));
        String tmp2 = "";
        tmp2 = Read(fi, 999, 0);
        String tmp = ConvertData(tmp2);
        info[4] = "Data:\n";
        info[5] = tmp2;
        if (info[0].contains("EXE"))
        {
            int codebyte = 0;
            String splitted[] = tmp2.split("\n");
            for (i = 0; i < splitted.length; i++)
            {
                tmp2 = tmp2.replace(splitted[i] + "\n", String.format("%03d: ", codebyte) + splitted[i] + "\n");
                codebyte += splitted[i].length() + 1;
            }
            info[5] = tmp2;
        }
        for (i = 0; i <= Math.ceil(tmp.length() / 32f) * 32; i += 2)
        {
            if (i != 0 && i % 8 == 0)
            {
                info[4] += "\t";
            }
            if (i != 0 && i % 32 == 0)
            {
                info[4] += "\n";
            }
            if (i <= tmp.length())
            {
                info[4] += tmp.substring(i, Math.min(i + 2, tmp.length())) + " ";
            }
            else
            {
                info[4] += "  ";
            }
        }
        return info;
    }

    public static FileInfo Open(String filepath, int flags, int pid) throws FileSystemException
    {
        FileInfo out = new FileInfo(0, 0, "0", pid);

        if (filepath.equals("/"))
        {
            addFileToList(out.fileID, flags);
            out.removable = true;
            return out;
        }

        out = Open(out, filepath, flags);

        ValidateAccess(out.fileID, pid, flags);

        if ((flags & F_APPEND) == F_APPEND)
        {
            ValidateAccess(out.fileID, pid, F_READ);
            out.filePtr = dysk.getFileSize(out.fileID);
        }
        if ((flags & F_ERASE) == F_ERASE)
        {
            Truncate(out, 0);
        }

        out.removable = true;
        return out;
    }

    public static FileInfo ChangeDirectory(FileInfo fi, String path) throws FileSystemException
    {
        Log.println("Otwieranie katalogu " + path + " wzgledem pliku (ID: " + fi.fileID + ")");
        FileInfo tempfi = new FileInfo(fi);
        tempfi = Open(tempfi, path, F_READ);
        if (IsFileDir(tempfi) == 0)
        {
            Log.println("Otwieranie nie powiodlo sie - nie mozna zmienic katalogu na plik.");
            throw new FileSystemException(3);
        }
        else
        {
            tempfi.removable = true;
            return tempfi;
        }
    }

    public static FileInfo Open(FileInfo fi, String path, int flags) throws FileSystemException
    {
        Log.println("Otwieranie pliku " + path);
        String newpath[] = path.split("/");
        Collections.reverse(Arrays.asList(newpath));
        if (newpath.length < 2 || path.contains("//"))
        {
            Log.println("Nieprawidlowa sciezka.");
            throw new FileSystemException(1);
        }
        fi = Open(fi, newpath, flags, newpath.length - 2);
        addFileToList(fi.fileID, flags);
        fi.removable = true;
        return fi;
    }

    private static FileInfo Open(FileInfo fi, String[] path, int flags, int ind) throws FileSystemException
    {
        if (path[ind].equals(".."))
        {
            fi.dirlist = fi.dirlist.replaceFirst("/[0-9]*$", "");
            String dirlist[] = fi.dirlist.split("/");
            fi.fileID = Integer.parseInt(dirlist[dirlist.length - 1]);
        }
        else
        {
            int dir_id = Integer.parseInt(fi.dirlist.split("/")[fi.dirlist.split("/").length - 1]);
            ValidateAccess(dir_id, fi.pid, F_READ);

            fi.fileID = FileExists(path[ind], dir_id, fi.pid);
            fi.dirlist += "/" + Integer.toString(fi.fileID);

            ValidateAccess(fi.fileID, fi.pid, flags);
        }
        if (ind > 0)
        {
            fi = Open(fi, path, flags, ind - 1);
        }
        return fi;
    }

    public static void Close(FileInfo fi)
    {
        if (fi != null)
        {
            Log.println("Zamykanie pliku (ID: " + fi.fileID + ").");
            fi.Close();
        }
    }

    public static boolean Delete(String path, int pid) throws FileSystemException
    {
        FileInfo todelete = Open(path, F_WRITE, pid);
        todelete.removable = false;
        return Delete(todelete);
    }

    public static boolean Delete(FileInfo fi) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        Log.println("Usuwanie pliku (ID: " + fi.fileID + ").");
        ValidateAccess(fi.fileID, fi.pid, F_READ | F_WRITE);
        dysk.nodes.unclaimID(fi.fileID);
        if ((dysk.getFileInfo(fi.fileID) & 512) >> 9 == 1)
        {
            DirEnt de = new DirEnt();
            for (int i = 0; i < dysk.getFileSize(fi.fileID);)
            {
                Read(fi, de, i);
                FileInfo tmp = new FileInfo(de.nodeID, 0, "", fi.pid);
                Delete(tmp);
                i += de.name.length() + 2;
            }
        }
        Truncate(fi, 0);
        dysk.writeFileNode(new Node(), fi.fileID);
        return true;
    }

    public static void ChangeFilePointer(FileInfo fi, int new_ptr) throws FileSystemException
    {
        if (new_ptr < GetFileSize(fi))
        {
            fi.filePtr = new_ptr;
        }
    }

    public static boolean Write(FileInfo fi, String text, int offset) throws FileSystemException
    {
        if (GetFileType(fi) == 4)
        {
            int dir_id = Integer.parseInt(fi.dirlist.split("/")[fi.dirlist.split("/").length - 2]);
            dc.write(ResolveName(fi.fileID, dir_id, fi.pid), text, offset);
            dysk.setFileSize(fi.fileID, offset + text.length());
            return true;
        }
        ChangeFilePointer(fi, offset);
        return Write(fi, text, false);
    }

    public static boolean Write(FileInfo fi, Object c, int offset) throws FileSystemException
    {
        ChangeFilePointer(fi, offset);
        return Write(fi, dysk.getClass(c), true);
    }

    public static boolean Write(FileInfo fi, Object c) throws FileSystemException
    {
        return Write(fi, dysk.getClass(c), true);
    }

    public static boolean Write(FileInfo fi, String text) throws FileSystemException
    {
        if (GetFileType(fi) == 4)
        {
            int dir_id = Integer.parseInt(fi.dirlist.split("/")[fi.dirlist.split("/").length - 2]);
            dc.write(ResolveName(fi.fileID, dir_id, fi.pid), text, 0);
            dysk.setFileSize(fi.fileID, 0 + text.length());
            return true;
        }
        return Write(fi, text, false);
    }

    private static boolean Write(FileInfo fi, String text, boolean isobject) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        Log.println("Zapisywanie do pliku (ID: " + fi.fileID + ") tekstu " + text.replace("\n", "\\n"));
        if (!isobject)
        {
            text = ConvertData(text);
        }
        if (text.length() != 0)
        {
            if (fi.removable == false || (pliki.get(Integer.toString(fi.fileID)).openMode & F_WRITE) == F_WRITE)
            {
                fi.filePtr += text.length() / 2;
                return dysk.writeToFile(fi.fileID, fi.filePtr - text.length() / 2, text);
            }
            else
            {
                Log.println("Zapis nie powiodl sie - operacja wymaga flagi F_WRITE.");
                throw new FileSystemException(6);
            }
        }
        else
        {
            Log.println("Zapis nie powiodl sie - brak tekstu do wpisania.");
            return false;
        }
    }

    public static void Read(FileInfo fi, Object c, int offset) throws FileSystemException
    {
        ChangeFilePointer(fi, offset);
        Read(fi, c);
    }

    public static String Read(FileInfo fi, int count, int offset) throws FileSystemException
    {
        if (GetFileType(fi) == 4)
        {
            int dir_id = Integer.parseInt(fi.dirlist.split("/")[fi.dirlist.split("/").length - 2]);
            return dc.read(ResolveName(fi.fileID, dir_id, 1), count, offset);
        }
        ChangeFilePointer(fi, offset);
        return Read(fi, count);
    }

    public static void Read(FileInfo fi, Object c) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        Log.println("Odczytywanie z pliku (ID: " + fi.fileID + "). Dane:");
        if (fi.removable == false || (pliki.get(Integer.toString(fi.fileID)).openMode & F_READ) == F_READ)
        {
            dysk.loadClassFromFile(c, fi.fileID, fi.filePtr);
            Log.println(dysk.getClass(c));
            fi.filePtr += dysk.classSize(c);
        }
        else
        {
            Log.println("Odczyt nie powiodl sie - operacja wymaga flagi F_READ.");
            throw new FileSystemException(5);
        }
        Log.println("");
    }

    public static String Read(FileInfo fi, int count) throws FileSystemException
    {
        if (fi == null)
        {
            throw new FileSystemException(4);
        }
        if (GetFileType(fi) == 4)
        {
            int dir_id = Integer.parseInt(fi.dirlist.split("/")[fi.dirlist.split("/").length - 2]);
            return dc.read(ResolveName(fi.fileID, dir_id, 1), count, 0);
        }
        Log.println("Odczytywanie z pliku (ID: " + fi.fileID + "). Dane:");
        if (count > 0)
        {
            if (fi.removable == false || (pliki.get(Integer.toString(fi.fileID)).openMode & F_READ) == F_READ)
            {
                fi.filePtr += count;
                String temp = "";
                temp = (String) RecoverData(dysk.readFromFile(fi.fileID, fi.filePtr - count, count), temp);
                Log.println(temp);
                return temp;
            }
            else
            {
                Log.println("Odczyt nie powiodl sie - operacja wymaga flagi F_READ.");
                throw new FileSystemException(5);
            }
        }
        else
        {
            Log.println("Odczyt nie powiodl sie - ilosc bajtow do odczytania nie moze byc mniejsza od 1.");
        }
        Log.println("");
        return "";
    }

    public static boolean Link(String filepath, String dirpath, String filename, int pid) throws FileSystemException
    {
        FileInfo file = Open(filepath, F_WRITE, pid);
        file.removable = false;
        FileInfo dir = Open(dirpath, F_WRITE, pid);
        dir.removable = false;
        return Link(file, dir, filename);
    }

    public static boolean Link(FileInfo file, FileInfo dir, String name) throws FileSystemException
    {
        if (file == null || dir == null)
        {
            Log.println("Tworzenie powiazania nie powiodlo sie - plik lub katalog nie istnieja.");
            throw new FileSystemException(4);
        }
        ValidateAccess(file.fileID, file.pid, F_WRITE);
        ValidateAccess(dir.fileID, dir.pid, F_WRITE);
        return Link(file.fileID, dir.fileID, name);
    }

    private static boolean Link(int file_id, int dir_id, String name)
    {
        Log.println("Tworzenie powiazania pliku (ID: " + file_id + ") w katalogu (ID: " + dir_id + ")");
        short link_count = (short) ((dysk.getFileInfo(file_id) & 7168) >> 10);
        if (link_count < 8)
        {
            dysk.setFileInfo(file_id, -1, link_count + 1, -1, -1);
            dysk.writeToFile(dir_id, -1, dysk.getClass(new DirEnt((byte) file_id, name)));
            return true;
        }
        return false;
    }

    public static boolean Unlink(String filepath, String dirpath, int pid) throws FileSystemException
    {
        FileInfo file = Open(filepath, F_WRITE, pid);
        file.removable = false;
        FileInfo dir = Open(dirpath, F_WRITE | F_READ, pid);
        dir.removable = false;
        return Unlink(file, dir);
    }

    public static boolean Unlink(FileInfo file, FileInfo dir) throws FileSystemException
    {
        if (file == null || dir == null)
        {
            Log.println("Zmiana nazwy nie powiodla sie - plik lub katalog nie istnieja.");
            throw new FileSystemException(4);
        }

        Log.println("Usuwanie powiazania pliku (ID: " + file.fileID + ") z katalogu (ID: " + dir.fileID + ")");
        short link_count = (short) ((dysk.getFileInfo(file.fileID) & 7168) >> 10);
        dysk.setFileInfo(file.fileID, -1, link_count - 1, -1, -1);
        String temp = ConvertData(Read(dir, 999, 0));
        DirEnt de = new DirEnt();
        for (int i = 0; i < dysk.getFileSize(dir.fileID);)
        {
            dysk.loadClassFromFile(de, dir.fileID, i);
            if (file.fileID == de.nodeID)
            {
                temp = temp.replaceFirst(dysk.getClass(de), "");
                break;
            }
            i += de.name.length() + 2;
        }
        Truncate(dir, 0);
        Write(dir, temp, true);
        if (link_count == 1)
        {
            Delete(file);
        }
        return true;
    }

    public static boolean Rename(String filepath, String dirpath, String newname, int pid) throws FileSystemException
    {
        FileInfo file = Open(filepath, F_READ, pid);
        file.removable = false;
        FileInfo dir = Open(dirpath, F_READ | F_WRITE, pid);
        dir.removable = false;
        return Rename(file, dir, newname);
    }

    public static boolean Rename(FileInfo file, FileInfo dir, String new_name) throws FileSystemException
    {
        if (file == null || dir == null)
        {
            Log.println("Zmiana nazwy nie powiodla sie - plik lub katalog nie istnieja.");
            throw new FileSystemException(4);
        }
        Log.println("Zmiana nazwy pliku (ID: " + file.fileID + "w katalogu (ID: " + dir.fileID);
        String temp = ConvertData(Read(dir, 999, 0));
        DirEnt de = new DirEnt();
        for (int i = 0; i < dysk.getFileSize(dir.fileID);)
        {
            Read(dir, de, i);
            if (file.fileID == de.nodeID)
            {
                temp = temp.replaceFirst(dysk.getClass(de), dysk.getClass(new DirEnt(de.nodeID, new_name)));
                break;
            }
            i += de.name.length() + 2;
        }
        Truncate(dir, 0);
        Write(dir, temp, true);
        return true;
    }

    public static boolean Truncate(FileInfo fi, int offset) throws FileSystemException
    {
        ChangeFilePointer(fi, offset);
        return Truncate(fi);
    }

    public static boolean Truncate(FileInfo fi) throws FileSystemException
    {
        if (fi == null)
        {
            Log.println("Usuwanie zawartosci pliku nie powiodlo sie poniewaz on nie istnieje.");
            throw new FileSystemException(4);
        }
        Log.println("Usuwanie zawartosci pliku (ID: " + fi.fileID + ").");

        ValidateAccess(fi.fileID, fi.pid, F_WRITE | F_READ);
        int i;
        for (i = dysk.getFileSize(fi.fileID); i >= fi.filePtr + 32; i--)
        {
            if (i % 32 == 0)
            {
                dysk.unclaimBlock(fi.fileID, i / dysk.mb.block_size);
                i -= 31;
            }
        }
        if (fi.filePtr == 0)
        {
            dysk.unclaimBlock(fi.fileID, 0);
        }
        else
        {
            Write(fi, dysk.truncator(i - fi.filePtr), fi.filePtr);
        }
        dysk.setFileSize(fi.fileID, fi.filePtr);
        return true;
    }

    private static String traverseDir(String path, int pid) throws FileSystemException
    {
        String out = "0";
        if (path.equals("/"))
        {
            return out;
        }
        ArrayList<String> lista = new ArrayList(Arrays.asList(path.split("/")));
        lista.set(0, "0");
        for (int i = 1; i < lista.size() - 1; i++)
        {
            if (lista.get(i).equals(".."))
            {
                out = out.replaceAll("/.*$", "");
                lista.remove(i);
                if (i != 1)
                {
                    lista.remove(i - 1);
                    i--;
                }
                i--;
            }
            else
            {
                ValidateAccess(Integer.parseInt(lista.get(i - 1)), pid, F_READ);
                lista.set(i, Integer.toString(FileExists(lista.get(i), Integer.parseInt(lista.get(i - 1)), pid)));
                out += "/" + lista.get(i);
            }
        }
        return out;
    }

    public static int FileExists(String path, int pid) throws FileSystemException
    {
        Log.println("Sprawdzanie czy plik " + path + " istnieje.");
        String dirlist = traverseDir(path, pid);
        int dir_id = Integer.parseInt(dirlist.split("/")[dirlist.split("/").length - 1]);
        DirEnt de = new DirEnt();
        ValidateAccess(dir_id, pid, F_READ);
        for (int i = 0; i < dysk.getFileSize(dir_id);)
        {
            dysk.loadClassFromFile(de, dir_id, i);
            if (path.split("/")[path.split("/").length - 1].equals(de.name))
            {
                if (((dysk.getFileInfo(de.nodeID) & -8192) >> 13) == 0)
                {
                    FileInfo file = new FileInfo(de.nodeID, 0, "", 0);
                    FileInfo dir = new FileInfo(dir_id, 0, "", 0);
                    Unlink(file, dir);
                    Log.println("Szukany plik nie zostal znaleziony.");
                    throw new FileSystemException(0);
                }
                return de.nodeID;
            }
            i += de.name.length() + 2;
        }
        Log.println("Szukany plik nie zostal znaleziony.");
        throw new FileSystemException(0);
    }

    public static int FileExists(String name, int dir_id, int pid) throws FileSystemException
    {
        Log.println("Sprawdzanie czy plik " + name + " istnieje w katalogu (ID: " + dir_id + "0.");
        DirEnt de = new DirEnt();
        ValidateAccess(dir_id, pid, F_READ);
        for (int i = 0; i < dysk.getFileSize(dir_id);)
        {
            dysk.loadClassFromFile(de, dir_id, i);
            if (name.equals(de.name))
            {
                if (((dysk.getFileInfo(de.nodeID) & -8192) >> 13) == 0)
                {
                    FileInfo file = new FileInfo(de.nodeID, 0, "", 0);
                    FileInfo dir = new FileInfo(dir_id, 0, "", 0);
                    Unlink(file, dir);
                    Log.println("Szukany plik nie zostal znaleziony.");
                    throw new FileSystemException(0);
                }
                return de.nodeID;
            }
            i += de.name.length() + 2;
        }
        Log.println("Szukany plik nie zostal znaleziony.");
        throw new FileSystemException(0);
    }

    private static boolean ValidateAccess(int id, int pid, int flags) throws FileSystemException
    {
        Log.println("Sprawdzanie czy mozna uzyskac dostep do pliku (ID: " + id + ") dla PID: " + pid);
        if (pid <= 1)
        {
            Log.println("Dostep przyznany.");
            return true;
        }
        int uid = pm.getUID(pid);
        int gid = pm.getGID(pid);
        if (uid == 0 && gid == 0)
        {
            Log.println("Dostep przyznany.");
            return true;
        }
        int fileuid = dysk.getFileUID(id);
        int filegid = dysk.getFileGID(id);
        if ((dysk.getFileInfo(id) & -8192) >> 13 == 0)
        {
            Log.println("Dostep przyznany.");
            return true;
        }
        short perms = (short) (dysk.getFileInfo(id) & 511);
        if ((flags & F_READ) == F_READ)
        {
            if (fileuid == uid)
            {
                if ((perms & 256) != 256)
                {
                    Log.println("Dostep zablokowany.");
                    throw new FileSystemException(2);
                }

            }
            else if (filegid == gid)
            {
                if ((perms & 32) != 32)
                {
                    Log.println("Dostep zablokowany.");
                    throw new FileSystemException(2);
                }
            }
            else if ((perms & 4) != 4)
            {
                Log.println("Dostep zablokowany.");
                throw new FileSystemException(2);
            }
        }
        if ((flags & F_WRITE) == F_WRITE)
        {
            if (fileuid == uid)
            {
                if ((perms & 128) != 128)
                {
                    Log.println("Dostep zablokowany.");
                    throw new FileSystemException(2);
                }

            }
            else if (filegid == gid)
            {
                if ((perms & 16) != 16)
                {
                    Log.println("Dostep zablokowany.");
                    throw new FileSystemException(2);
                }
            }
            else if ((perms & 2) != 2)
            {
                Log.println("Dostep zablokowany.");
                throw new FileSystemException(2);
            }
        }
        if ((dysk.getFileInfo(id) & -8192) == 16384)
        {
            if (fileuid == uid)
            {
                if ((perms & 64) != 64)
                {
                    Log.println("Dostep zablokowany.");
                    throw new FileSystemException(2);
                }

            }
            else if (filegid == gid)
            {
                if ((perms & 8) != 8)
                {
                    Log.println("Dostep zablokowany.");
                    throw new FileSystemException(2);
                }
            }
            else if ((perms & 1) != 1)
            {
                Log.println("Dostep zablokowany.");
                throw new FileSystemException(2);
            }
        }
        Log.println("Dostep przyznany.");
        return true;
    }

    public static boolean CreateDir(String path, int pid, int perms) throws FileSystemException
    {
        return CreateDir(path, pid, pm.getUID(pid), pm.getGID(pid), perms);
    }

    public static boolean CreateDir(String path, int pid, int uid, int gid, int perms) throws FileSystemException
    {
        Log.println("Tworzenie katalogu " + path);
        if (dysk.mb.nodes_available == 0)
        {
            Log.println("Tworzenie katalogu nie powiodlo sie - brak wolnych wezlow.");
            throw new FileSystemException(7);
        }
        String dirlist = traverseDir(path, pid);
        int previous = Integer.parseInt(dirlist.split("/")[dirlist.split("/").length - 1]);
        int id = -1;
        try
        {
            id = FileExists(path, pid);
        }
        catch (FileSystemException ex)
        {
            if (ex.errorCode() == 0 && previous != -1)
            {
                id = dysk.writeFileNode(dysk.getFileInfoNode(1, 1, perms, uid, gid), -1);
                Link(id, previous, path.split("/")[path.split("/").length - 1]);
            }
        }
        return (id != -1);
    }

    public static boolean CreateFile(String path, int pid, int perms) throws FileSystemException
    {
        return CreateFile(path, pid, pm.getUID(pid), pm.getGID(pid), perms);
    }

    public static boolean CreateFile(String path, int pid, int uid, int gid, int perms) throws FileSystemException
    {
        Log.println("Tworzenie pliku " + path);
        if (dysk.mb.nodes_available == 0)
        {
            Log.println("Tworzenie pliku nie powiodlo sie - brak wolnych wezlow.");
            throw new FileSystemException(7);
        }
        String dirlist = traverseDir(path, pid);
        int previous = Integer.parseInt(dirlist.split("/")[dirlist.split("/").length - 1]);
        int id = -1;
        try
        {
            id = FileExists(path, pid);
        }
        catch (FileSystemException ex)
        {
            if (ex.errorCode() == 0 && previous != -1)
            {
                id = dysk.writeFileNode(dysk.getFileInfoNode(2, 0, perms, uid, gid), -1);
                Link(id, previous, path.split("/")[path.split("/").length - 1]);
            }
        }
        return (id != -1);
    }

    private static void addFileToList(int fid, int flags)
    {
        String temp = Integer.toString(fid);
        if (pliki.containsKey(temp))
        {
            int count = pliki.get(temp).openCount + 1;
            pliki.put(temp, new GlobalFileInfo(count, flags | pliki.get(temp).openMode));
        }
        else
        {
            pliki.put(temp, new GlobalFileInfo(1, flags));
        }
    }

    static void removeFileFromList(int fid)
    {
        String temp = Integer.toString(fid);
        if (pliki.containsKey(temp))
        {
            int count = pliki.get(temp).openCount - 1;
            if (count == 0)
            {
                pliki.remove(temp);
                Log.println("Usuwanie pliku (ID: " + fid + ") z listy");
            }
            else
            {
                pliki.put(temp, new GlobalFileInfo(count, pliki.get(temp).openMode));
            }
        }
    }

    public static void SaveFromStrings(String[] tab)
    {
        FileInfo tmp;
        for (int i = 0; i < tab.length; i++)
        {
            try
            {
                CreateFile("/p" + i, 1, 747);
            }
            catch (FileSystemException ex)
            {
                Log.println(ex);
            }
            try
            {
                tmp = Open("/p" + i, F_WRITE, 1);
                SetFileType(tmp, 3);
                Write(tmp, tab[i]);
                Close(tmp);
            }
            catch (FileSystemException ex)
            {
                Log.println(ex);
            }
        }
    }

    public FileSystem()
    {
        dysk = new FileDisk("dysk.txt");
        pliki = new TreeMap();
        Log = new Window("FileSystem Log", 400, 300);
    }
}
