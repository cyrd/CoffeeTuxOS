package coffeetux.filesystem;

class GlobalFileInfo
{

    public int openCount;
    public int openMode;

    public GlobalFileInfo(int oc, int om)
    {
        openCount = oc;
        openMode = om;
    }
}
