package coffeetux.filesystem;

public class SizeOf
{

    public static int SizeOf(String v)
    {
        v = v.replaceAll("class ", "").replaceAll("java.lang.", "").toUpperCase();
        if (v.compareTo("BYTE") == 0 || v.compareTo("[B") == 0)
        {
            return 1;
        }
        if (v.compareTo("SHORT") == 0 || v.compareTo("[S") == 0)
        {
            return 2;
        }
        if (v.compareTo("INT") == 0 || v.compareTo("INTEGER") == 0 || v.compareTo("[I") == 0)
        {
            return 4;
        }
        if (v.compareTo("FLOAT") == 0 || v.compareTo("[F") == 0)
        {
            return 4;
        }
        if (v.compareTo("LONG") == 0 || v.compareTo("[L") == 0)
        {
            return 8;
        }
        if (v.compareTo("DOUBLE") == 0 || v.compareTo("[D") == 0)
        {
            return 8;
        }
        return 0;
    }

    public static int SizeOf(byte v)
    {
        return 1;
    }

    public static int SizeOf(short v)
    {
        return 2;
    }

    public static int SizeOf(int v)
    {
        return 4;
    }

    public static int SizeOf(long v)
    {
        return 8;
    }

    public static int SizeOf(float v)
    {
        return 4;
    }

    public static int SizeOf(double v)
    {
        return 8;
    }
}
