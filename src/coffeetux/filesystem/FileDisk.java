package coffeetux.filesystem;

import static coffeetux.filesystem.Converter.*;
import static coffeetux.filesystem.SizeOf.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Array;
import java.lang.reflect.Field;

class FileDisk
{

    RandomAccessFile plik = null;
    MainBlock mb;
    VecBin nodes, blocks;

    public FileDisk(String nazwa)
    {
        mb = new MainBlock();
        nodes = new VecBin(this, mb.node_binvecs_num);
        blocks = new VecBin(this, mb.block_binvecs_num);
        try
        {
            if (!(new File(nazwa).exists()))
            {
                plik = new RandomAccessFile(nazwa, "rw");

                mb.calculate();

                saveClass(getClass(mb), 0);
                saveClass(getClass(nodes), mb.binvec_nodes_offset);
                saveClass(getClass(blocks), mb.binvec_blocks_offset);

                for (int i = 0; i < mb.nodes_available; i++)
                {
                    saveClass(getClass(new Node()), getNodeByte(i));
                }
                for (int i = 0; i < mb.blocks_available; i++)
                {
                    for (int j = 0; j < mb.block_size; j++)
                    {
                        writeInfo("00");
                    }
                }
                writeFileNode(getFileInfoNode(1, 1, 764, 0, 0), -1);
            }
            else
            {
                plik = new RandomAccessFile(nazwa, "rw");
                loadClass(mb, 0);
                loadClass(nodes, mb.binvec_nodes_offset);
                loadClass(blocks, mb.binvec_blocks_offset);
            }
        }
        catch (FileNotFoundException fnf)
        {
        }
    }

    public void shutdown()
    {
        changePosition(0);
        saveClass(getClass(mb), 0);
        saveClass(getClass(nodes), mb.binvec_nodes_offset);
        saveClass(getClass(blocks), mb.binvec_blocks_offset);
        try
        {
            plik.close();
        }
        catch (IOException io)
        {
            System.out.println("FileDisk (finalize): " + io.getStackTrace());
        }
    }

    protected void finalize()
    {
        shutdown();
    }

    private boolean writeInfo(String v)
    {
        try
        {
            plik.writeBytes(v);
        }
        catch (IOException io)
        {
            System.out.println("FileDisk (writeInfo):" + io.getStackTrace());
            return false;
        }
        return true;
    }

    private String readInfo(int bytes_count)
    {
        byte[] tab = new byte[bytes_count * 2];
        try
        {
            plik.read(tab);
        }
        catch (IOException io)
        {
            System.out.println("FileDisk (readInfo):" + io.getStackTrace());
        }
        return stringify_tab(tab);
    }

    private boolean changePosition(long byte_number)
    {
        try
        {
            plik.seek(byte_number * 2);
        }
        catch (IOException io)
        {
            System.out.println("FileDisk (changePosition):" + io.getStackTrace());
            return false;
        }
        return true;
    }

    int getNodeByte(int id)
    {
        return id * mb.node_size + mb.nodes_offset;
    }

    int getBlockByte(int id)
    {
        return id * mb.block_size + mb.blocks_offset;
    }

    Node getFileInfoNode(int file_type, int isdir, int perms, int uid, int gid)
    {
        Node out = new Node();
        out.file_info |= file_type << 13;
        out.file_info |= isdir << 9;
        out.file_info |= Integer.parseInt(Integer.toString(perms), 8);
        out.uid = (byte) uid;
        out.gid = (byte) gid;
        return out;
    }

    int writeFileNode(Node file, int id)
    {
        if (id == -1)
        {
            int id2 = nodes.findFreeID();
            if (id2 != -1)
            {
                mb.nodes_free--;
                nodes.claimID(id2);
                saveClass(getClass(file), getNodeByte(id2));
                return id2;
            }
        }
        else
        {
            saveClass(getClass(file), getNodeByte(id));
            return id;
        }
        return -1;
    }

    Node getFileNode(int id)
    {
        Node out = new Node();
        loadClass(out, getNodeByte(id));
        return out;
    }

    short setFileInfo(int id, int file_type, int link_count, int isdir, int perms)
    {
        changePosition(getNodeByte(id));
        short temp = (short) RecoverData(readInfo(2), new Short("0"));
        if (file_type != -1)
        {
            temp &= 8191;
            temp |= file_type << 13;
        }
        if (link_count != -1)
        {
            temp &= -7169;
            temp |= link_count << 10;
        }
        if (isdir != -1)
        {
            temp &= -513;
            temp |= isdir << 9;
        }
        if (perms != -1)
        {
            temp &= -512;
            temp |= Integer.parseInt(Integer.toString(perms), 8);
        }
        changePosition(getNodeByte(id));
        writeInfo(ConvertData(temp));
        return temp;
    }

    short getFileInfo(int id)
    {
        changePosition(getNodeByte(id));
        return (short) RecoverData(readInfo(2), new Short("0"));
    }

    void setFileUID(int id, int uid)
    {
        changePosition(getNodeByte(id) + 2);
        writeInfo(ConvertData((byte) uid));
    }

    byte getFileUID(int id)
    {
        changePosition(getNodeByte(id) + 2);
        return (byte) RecoverData(readInfo(1), new Byte("0"));
    }

    void setFileGID(int id, int gid)
    {
        changePosition(getNodeByte(id) + 3);
        writeInfo(ConvertData((byte) gid));
    }

    byte getFileGID(int id)
    {
        changePosition(getNodeByte(id) + 3);
        return (byte) RecoverData(readInfo(1), new Byte("0"));
    }

    void setFileSize(int id, int size)
    {
        changePosition(getNodeByte(id) + 4);
        writeInfo(ConvertData((short) size));
    }

    short getFileSize(int id)
    {
        changePosition(getNodeByte(id) + 4);
        return (short) RecoverData(readInfo(2), new Short("0"));
    }

    short getNodeBlockID(int node_id, int block)
    {
        if (block <= 2 && block >= 0)
        {
            changePosition(getNodeByte(node_id) + 6 + block * 2);
            return (short) RecoverData(readInfo(2), new Short("0"));
        }
        if (block > 2)
        {
            changePosition(getNodeByte(node_id) + 12);
            short temp = (short) (RecoverData(readInfo(2), new Short("0")));
            if (temp == -1)
            {
                return -1;
            }
            changePosition(getBlockByte(temp) + (block - 3) * 2);
            return (short) RecoverData(readInfo(2), new Short("0"));
        }
        return -1;
    }

    void claimBlock(int node_id, int block)
    {
        int block_id = 0;
        if (block <= 2 && block >= 0)
        {
            changePosition(getNodeByte(node_id) + 6 + block * 2);
        }
        if (block > 2)
        {
            changePosition(getNodeByte(node_id) + 12);
            block_id = (short) RecoverData(readInfo(2), new Short("0"));
            if ((short) block_id == -1)
            {
                block_id = blocks.findFreeID();
                blocks.claimID(block_id);
                mb.blocks_free--;
                changePosition(getNodeByte(node_id) + 12);
                writeInfo(ConvertData((short) block_id));
                changePosition(getBlockByte(block_id));
                for (int i = 0; i < 16; i++)
                {
                    writeInfo(ConvertData((short) -1));
                }
            }
            changePosition(getBlockByte(block_id) + (block - 3) * 2);
        }
        block_id = blocks.findFreeID();
        if (block_id != -1)
        {
            blocks.claimID(block_id);
            mb.blocks_free--;
            writeInfo(ConvertData((short) block_id));
        }
    }

    String truncator(int count)
    {
        String out = "";
        for (int i = 0; i < count; i++)
        {
            out += "00";
        }
        return out;
    }

    void unclaimBlock(int node_id, int block)
    {
        int block_id = 0;
        int indirect_block = 0;
        if (block <= 2 && block >= 0)
        {
            changePosition(getNodeByte(node_id) + 6 + block * 2);
            block_id = (short) RecoverData(readInfo(2), new Short("0"));
            changePosition(getNodeByte(node_id) + 6 + block * 2);
        }
        if (block > 2)
        {
            changePosition(getNodeByte(node_id) + 12);
            indirect_block = (short) RecoverData(readInfo(2), new Short("0"));
            changePosition(getBlockByte(indirect_block) + (block - 3) * 2);
            block_id = (short) RecoverData(readInfo(2), new Short("0"));
            changePosition(getBlockByte(indirect_block) + (block - 3) * 2);
        }
        if (block_id != -1)
        {
            blocks.unclaimID(block_id);
            mb.blocks_free++;
            writeInfo(ConvertData((short) -1));
            changePosition(getBlockByte(block_id));
            writeInfo(truncator(32));
        }
        if (block == 3)
        {
            blocks.unclaimID(indirect_block);
            mb.blocks_free++;
            changePosition(getNodeByte(node_id) + 12);
            writeInfo(ConvertData((short) -1));
            changePosition(getBlockByte(indirect_block));
            writeInfo(truncator(32));
        }
    }

    boolean writeToFile(int id, int byte_nr, String data)
    {
        if (byte_nr / mb.block_size < 8)
        {
            if (byte_nr == -1)
            {
                byte_nr = getFileSize(id);
            }
            setFileSize(id, (int) Math.ceil(byte_nr + data.length() / 2f));
            if (getNodeBlockID(id, byte_nr / mb.block_size) == -1)
            {
                claimBlock(id, byte_nr / mb.block_size);
            }
            if (!changePosition(getBlockByte(getNodeBlockID(id, byte_nr / mb.block_size)) + byte_nr % 32))
            {
                return false;
            }
            for (int i = byte_nr; i < Math.ceil(data.length() / 2f) + byte_nr; i++)
            {
                if (i % 32 == 0)
                {
                    if (getNodeBlockID(id, i / mb.block_size) == -1 && i / mb.block_size < 8)
                    {
                        claimBlock(id, i / mb.block_size);
                    }
                    if (!changePosition(getBlockByte(getNodeBlockID(id, i / mb.block_size))))
                    {
                        return false;
                    }
                }
                if (!writeInfo(data.substring((i - byte_nr) * 2, Integer.min((i - byte_nr) * 2 + 2, data.length()))))
                {
                    return false;
                }
            }
            return true;
        }
        return false;

    }

    String readFromFile(int id, int byte_nr, int count)
    {
        String temp = "";
        if (count == 999)
        {
            count = getFileSize(id);
        }
        changePosition(getBlockByte(getNodeBlockID(id, byte_nr / mb.block_size)) + byte_nr % 32);
        for (int i = byte_nr; i < count + byte_nr; i++)
        {
            if (i % 32 == 0)
            {
                if (getNodeBlockID(id, i / mb.block_size) != -1 && i / mb.block_size < 8)
                {
                    changePosition(getBlockByte(getNodeBlockID(id, i / mb.block_size)));
                }
            }
            String val = readInfo(1);
            temp += val;
        }
        return temp.substring(0, count * 2);
    }

    private String stringify_tab(byte[] tab)
    {
        String temp = new String();
        for (int i = 0; i < tab.length; i++)
        {
            temp += (char) tab[i];
        }
        return temp;
    }

    private void saveClass(String s, long file_ptr)
    {
        changePosition(file_ptr);
        writeInfo(s);
    }

    boolean loadClassFromFile(Object c, int id, int byte_nr)
    {
        int size = 0;
        c = performWrap(c);
        Field[] f = c.getClass().getFields();
        for (Field i : f)
        {
            Object val;
            try
            {
                val = i.get(c);
                if (val.getClass().toString().replaceAll("class java.lang.", "").compareTo("String") == 0)
                {
                    byte strsize = new Byte("0");
                    strsize = (byte) RecoverData(readFromFile(id, byte_nr + size, 1), strsize);
                    size++;
                    size += strsize;
                }
                else
                {
                    if (val.getClass().isArray())
                    {
                        for (int j = 0; j < Array.getLength(val); j++)
                        {
                            size += SizeOf(val.getClass().toString());
                        }
                    }
                    else
                    {
                        size += SizeOf(i.getType().toString());
                    }
                }
            }
            catch (IllegalAccessException ia)
            {
                System.out.println("FileDisk (classSize):" + ia.getStackTrace());
            }
        }
        return recoverClass(c, readFromFile(id, byte_nr, size));
    }

    private boolean loadClass(Object c, long file_ptr)
    {
        changePosition(file_ptr);
        return recoverClass(c, readInfo(classSize(c)));
    }

    String getClass(Object c)
    {
        String out = "";
        c = performWrap(c);
        byte tmp;
        Field[] f = c.getClass().getFields();
        for (Field i : f)
        {
            Object val = 0;
            try
            {
                val = i.get(c);
            }
            catch (IllegalAccessException ia)
            {
                return "-";
            }
            if (val.getClass().toString().replaceAll("class java.lang.", "").compareTo("String") == 0)
            {
                String temp = (String) val;
                tmp = (byte) temp.length();
                out += ConvertData((byte) temp.length());
            }
            if (val.getClass().isArray())
            {
                for (int j = 0; j < Array.getLength(val); j++)
                {
                    Object element = Array.get(val, j);
                    out += ConvertData(element);
                }
            }
            else
            {
                out += ConvertData(val);
            }

        }
        return out;
    }

    boolean recoverClass(Object c, String classtext)
    {
        int pos = 0;
        c = performWrap(c);
        Field[] f = c.getClass().getFields();
        for (Field i : f)
        {
            Object val;
            try
            {
                val = i.get(c);
                if (val.getClass().toString().replaceAll("class java.lang.", "").compareTo("String") == 0)
                {
                    byte size = new Byte("0");
                    size = (byte) RecoverData(classtext.substring(pos, pos + 2), size);
                    pos += 2;
                    String temp = "";
                    temp = (String) (RecoverData(classtext.substring(pos, pos + size * 2), temp));
                    pos += size;
                    val = temp;
                }
                else
                {
                    if (val.getClass().isArray())
                    {
                        for (int j = 0; j < Array.getLength(val); j++)
                        {
                            Object temp = Array.get(val, j);
                            temp = RecoverData(classtext.substring(pos, pos + 2 * SizeOf(val.getClass().toString())), temp);
                            pos += 2 * SizeOf(val.getClass().toString());
                            Array.set(val, j, temp);
                        }
                    }
                    else
                    {
                        val = RecoverData(classtext.substring(pos, pos + 2 * SizeOf(i.getType().toString())), val);
                        pos += 2 * SizeOf(i.getType().toString());
                    }
                }
                i.set(c, val);
            }
            catch (IllegalAccessException ia)
            {
                System.out.println("FileDisk (recoverClass):" + ia.getStackTrace());
                return false;
            }
        }
        return true;
    }

    int classSize(Object c)
    {
        int size = 0;
        c = performWrap(c);
        Field[] f = c.getClass().getFields();
        for (Field i : f)
        {
            Object val;
            try
            {
                val = i.get(c);
                if (val.getClass().toString().replaceAll("class java.lang.", "").compareTo("String") == 0)
                {
                    size += 1 + ((String) val).length();
                }
                else
                {
                    if (val.getClass().isArray())
                    {
                        for (int j = 0; j < Array.getLength(val); j++)
                        {
                            size += SizeOf(val.getClass().toString());
                        }
                    }
                    else
                    {
                        size += SizeOf(i.getType().toString());
                    }
                }
                i.set(c, val);
            }
            catch (IllegalAccessException ia)
            {
                System.out.println("FileDisk (classSize):" + ia.getStackTrace());
                return 0;
            }
        }
        return size;
    }
}
