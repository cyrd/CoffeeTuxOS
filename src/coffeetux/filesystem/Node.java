package coffeetux.filesystem;

//import static utils.SizeOf.SizeOf;

class Node
{

    public short file_info;
    public byte uid;
    public byte gid;
    public short file_size;
    public short direct_blocks[];
    public short indirect_block = -1;

    public Node()
    {
        direct_blocks = new short[3];
        for (int i = 0; i < direct_blocks.length; i++)
        {
            direct_blocks[i] = -1;
        }
    }

    public static byte sizeOf()
    {
        return 14;
    }
;
}
