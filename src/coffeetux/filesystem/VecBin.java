package coffeetux.filesystem;

class VecBin
{

    byte count;
    public int[] numbers;
    FileDisk fd;

    public VecBin(FileDisk fd, int count)
    {
        this.fd = fd;
        this.count = (byte) count;
        numbers = new int[count];
    }

    public boolean checkID(int id)
    {
        if ((numbers[id / 32] >> (id % 32) & 1) == 1)
        {
            return true;
        }
        return false;
    }

    public void claimID(int id)
    {
        numbers[id / 32] |= (long) Math.pow(2, id % 32);
    }

    public void unclaimID(int id)
    {
        numbers[id / 32] &= (long) (-1 - Math.pow(2, id % 32));
    }

    public int findFreeID()
    {
        for (int i2 = 0; i2 < 32 * count; i2++)
        {
            if (!checkID(i2))
            {
                return i2;
            }
        }

        return -1;
    }
}
