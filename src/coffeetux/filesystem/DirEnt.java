package coffeetux.filesystem;

class DirEnt
{

    public byte nodeID;
    public String name;

    public DirEnt(byte nid, String n)
    {
        nodeID = nid;
        name = n;
    }

    public DirEnt()
    {
        nodeID = 0;
        name = "";
    }
}
