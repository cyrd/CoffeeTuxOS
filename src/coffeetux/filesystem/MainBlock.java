package coffeetux.filesystem;
import static coffeetux.filesystem.SizeOf.SizeOf;
//import static utils.SizeOf.SizeOf;

class MainBlock
{

    public byte nodes_available = 64;
    public byte nodes_free = 64;
    public byte node_size = Node.sizeOf();
    public short blocks_available = 192;
    public short blocks_free = 192;
    public byte block_size = 32;
    public byte node_binvecs_num = 2;
    public byte node_binvec_size = 4;
    public byte block_binvecs_num = 6;
    public byte block_binvec_size = 4;

    public byte binvec_nodes_offset = 0;
    public byte binvec_blocks_offset = 0;
    public short nodes_offset = 0;
    public short blocks_offset = 0;

    public void calculate()
    {
        binvec_nodes_offset += SizeOf(nodes_available) + SizeOf(nodes_free) + SizeOf(node_size) + SizeOf(blocks_available) + SizeOf(blocks_free) + SizeOf(block_size) + SizeOf(node_binvecs_num) + SizeOf(node_binvec_size) + SizeOf(block_binvecs_num) + SizeOf(block_binvec_size) + SizeOf(binvec_nodes_offset) + SizeOf(binvec_blocks_offset) + SizeOf(nodes_offset) + SizeOf(blocks_offset);
        binvec_blocks_offset += binvec_nodes_offset + node_binvecs_num * node_binvec_size;
        nodes_offset += block_binvecs_num * block_binvec_size + binvec_blocks_offset;
        blocks_offset += nodes_available * node_size + nodes_offset;
    }
}
