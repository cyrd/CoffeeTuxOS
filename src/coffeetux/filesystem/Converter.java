package coffeetux.filesystem;

public class Converter
{

    public static String ConvertData(Object s)
    {
        if (s instanceof Byte)
        {
            return dec2hex((byte) s);
        }
        if (s instanceof Short)
        {
            return dec2hex((short) s);
        }
        if (s instanceof Integer)
        {
            return dec2hex((int) s);
        }
        if (s instanceof Long)
        {
            return dec2hex((long) s);
        }
        if (s instanceof Float)
        {
            return dec2hex((float) s);
        }
        if (s instanceof Double)
        {
            return dec2hex((double) s);
        }
        if (s instanceof String)
        {
            String tmp = (String) s;
            String out = "";
            for (int i = 0; i < tmp.length(); i++)
            {
                out += dec2hex((byte) tmp.charAt(i));
            }
            return out;
        }
        return s.toString();
    }

    public static Object RecoverData(String v, Object s)
    {
        if (s instanceof Byte)
        {
            s = (byte) hex2dec(v);
        }
        if (s instanceof Short)
        {
            s = (short) hex2dec(v);
        }
        if (s instanceof Integer)
        {
            s = (int) hex2dec(v);
        }
        if (s instanceof Long)
        {
            s = hex2dec(v);
        }
        if (s instanceof Float)
        {
            s = Float.intBitsToFloat((int) hex2dec(v));
        }
        if (s instanceof Double)
        {
            s = Double.longBitsToDouble(hex2dec(v));
        }
        if (s instanceof String)
        {
            String tmp = (String) v;
            String out = "";
            for (int i = 0; i < tmp.length() / 2; i++)
            {
                out += (char) hex2dec(tmp.substring(i * 2, i * 2 + 2));
            }
            return out;
        }
        return s;
    }

    public static Object performWrap(Object s)
    {
        if (s instanceof byte[] || s instanceof short[] || s instanceof int[] || s instanceof long[] || s instanceof float[] || s instanceof double[])
        {
            return new Wrapper(s);
        }
        return s;
    }

    private static String dec2hex(byte v)
    {
        return hexify((short) (v & 0xFF));
    }

    private static String dec2hex(short v)
    {
        String temp = new String();
        for (int i = 0; i < 2; i++)
        {
            temp += hexify((short) (v & 0xFF));
            v >>= 8;
        }
        return temp;
    }

    private static String dec2hex(int v)
    {
        String temp = new String();
        for (int i = 0; i < 4; i++)
        {
            temp += hexify((short) (v & 0xFF));
            v >>= 8;
        }
        return temp;
    }

    private static String dec2hex(float v)
    {
        String temp = new String();
        int temp2 = Float.floatToIntBits(v);
        for (int i = 0; i < 4; i++)
        {
            temp += hexify((short) (temp2 & 0xFF));
            temp2 >>= 8;
        }
        return temp;
    }

    private static String dec2hex(long v)
    {
        String temp = new String();
        for (int i = 0; i < 8; i++)
        {
            temp += hexify((short) (v & 0xFF));
            v >>= 8;
        }
        return temp;
    }

    private static String dec2hex(double v)
    {
        String temp = new String();
        long temp2 = Double.doubleToLongBits(v);
        for (int i = 0; i < 8; i++)
        {
            temp += hexify((short) (temp2 & 0xFF));
            temp2 >>= 8;
        }
        return temp;
    }

    private static long hex2dec(String v)
    {
        long temp = 0;
        for (int i = v.length() / 2 - 1; i >= 0; i--)
        {
            temp |= Integer.parseInt(v.substring(i * 2, i * 2 + 2), 16);
            if (i != 0)
            {
                temp <<= 8;
            }
        }
        return temp;
    }

    private static String hexify(short v)
    {
        String temp = new String();
        if (v < 16)
        {
            temp += "0";
            if (v == 0)
            {
                temp += "0";
            }
        }
        if (v != 0)
        {
            temp += Integer.toHexString(v);
        }
        return temp.toUpperCase();
    }
}
