package coffeetux.CPU;

import java.util.ArrayList;
import coffeetux.filesystem.*;
import coffeetux.Process_Management.*;

/**
 *
 * @author slawek
 */
public class PCB implements Comparable<PCB>
{

    public int PID;
    public int PPID;
    public int UID;
    public int GID;
    
    public int process_status;
    public int credit;
    public int priority;
    public ArrayList<Page_tab> pageTable;
    public FileInfo fileInfo;
    // TO DO:
    //FileInfo FS; // dane dotyczacego pliku na dysku
    //ArrayList<Page> PageTable = new ArrayList();
    // PPID, UID, GID etc. etc.

    // konstruktor klasy PCB musi uwzledniac wszystkie pola, fileinfo etc.
    public PCB(int pid, int setpriority, char[] mem, int i)
    {
        PID = pid;
        priority = setpriority;
        credit = priority;//  memory=mem;
        // offset = i;
    }

    public PCB(int pid, int setpriority)
    { // constructor for not assambly-like process (SHELL, INIT)
        PID = pid;
        priority = setpriority;
    }

    public PCB()
    {

    }

    // context
    int A;
    int B;
    int C;
    int D;
    int CompareFlag;
    int PC;

    int getA()
    {
        return A;
    }

    int getB()
    {
        return B;
    }

    int getC()
    {
        return C;
    }

    int getD()
    {
        return D;
    }

    int getCF()
    {
        return CompareFlag;
    }

    int getPC()
    {
        return PC;
    }

    @Override
    public int compareTo(PCB o)
    {
        int comparedCredit = o.credit;
        if (this.credit > comparedCredit)
        {
            return 1;
        } else if (this.credit == comparedCredit)
        {
            return 0;
        } else
        {
            return -1;
        }
    }

    @Override
    public String toString()
    { //used to diplay list
        return "\tPID=" + PID + "\tUID=" + UID +  "\tPriority=" + priority + "\tCredit=" + Integer.toString(credit) +"\t" + A +"\t"+ B +"\t"+ C +"\t"+ D +"\t"+ CompareFlag +"\t"+ PC + "\n";
    }

    void SetContext(int a, int b, int c, int d, int cf, int pc)
    {
        A = a;
        B = b;
        C = c;
        D = d;
        CompareFlag = cf;
        PC = pc;
    }

    void SetCredit(int value)
    {
        credit = value;
    }

    void SetStatus(int stat)
    {
        process_status = stat;
    }

    int GetCredit()
    {
        return credit;
    }

    int GetPriority()
    {
        return priority;
    }

    int GetPID()
    {
        return PID;
    }

    int GetStatus()
    {
        return process_status;
    }

    void Display()
    {
        System.out.print("PID=" + PID + ". Kredyt=" + credit + ". Priorytet=" + priority + "\n");
    }
}
