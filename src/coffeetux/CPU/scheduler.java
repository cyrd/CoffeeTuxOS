/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffeetux.CPU;

//import coffeetux.CPU.*;
//import CPU.*;
import coffeetux.Process_Management.Process_Management;
import java.util.LinkedList;
import java.util.List;
import java.util.Collections;
import Console.Window;
import java.io.IOException;

/**
 *
 * @author slawomir talarczyk
 */
public class scheduler
{

    public Process_Management processes = null;
    long TimeCounter = System.currentTimeMillis();

    public void take(List<PCB> ReadyProcess, List<PCB> SuspendedProcess, CPU processor)
    {

        try
            {
            System.out.println("Press enter to continue");
            System.in.read();
            } catch (IOException x)
            {
            };
        //while (ReadyProcess.get(0).PID != 1)
        while (ReadyProcess.isEmpty() != true)
        {
            if (System.currentTimeMillis() - TimeCounter > 10000)
            {
                ReadyProcess.get(0).SetCredit(ReadyProcess.get(0).GetCredit() / 2);
            }
            if (ReadyProcess.get(0).GetStatus() == 4)
            {
                processes.exit(ReadyProcess.get(0).PID);
                //ReadyProcess.remove(0);
            }

            if (ReadyProcess.get(0).GetStatus() != 4)
            {
                processor.console.println("[SCHEDULER] Executing process " + ReadyProcess.get(0).GetPID());
                ReadyProcess.set(0, processor.run(ReadyProcess.get(0))); // run process and return context
            }
            //if(ReadyProcess.get(0).PID != 1)
            SuspendedProcess.add(ReadyProcess.get(0));
            if (!ReadyProcess.isEmpty())
            {
                ReadyProcess.remove(0);
            }
          
            
          

        }
        TimeCounter = System.currentTimeMillis();
        NewAge(ReadyProcess);
        NewAge(SuspendedProcess);
        processor.console.println("[SCHEDULER] Recalculating new age");
        List<PCB> tmp = new LinkedList<PCB>(ReadyProcess);
        ReadyProcess.clear();
        ReadyProcess.addAll(SuspendedProcess);
        SuspendedProcess.clear();
        SuspendedProcess.addAll(tmp);
        Collections.sort(ReadyProcess);
        processor.console.println("[SCHEDULER] Process List: \t\t\t\tA\tB\tC\tD\tCF\tPC");
        processor.console.println(ReadyProcess + "\n");
    }

    void NewAge(List<PCB> ProcessList)
    {
        for (PCB i : ProcessList)
        {
            int credit = i.GetCredit();
            int priority = i.GetPriority();
            int newcredit = (credit / 2 + priority);
            if (newcredit > 39)
            {
                newcredit = 39;
            }
            i.SetCredit(newcredit);
            //System.out.println(credit +"/2+"+ priority  + "=" + newcredit);
        }
    }

}
