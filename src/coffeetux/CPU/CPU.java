package coffeetux.CPU;

import java.lang.String;
import coffeetux.Process_Management.Process_Management;
import coffeetux.Shell.Shell;
import coffeetux.memory.*;
import Console.Window;
import coffeetux.Shell.LoginManager;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author slawomir talarczyk
 */
public class CPU
{

    //registers
    int A;
    int B;
    int C;
    int D;
    int CompareFlag;
    int PC;

    public Shell shell = null;
    public Memory memory = null;
    public Process_Management pr = null;

    public Window console;

    char GetByte(int i, PCB proces)
    {
        return memory.MemoryRead(proces.GetPID(), i / 32, i % 32, proces.pageTable, proces.fileInfo);
    }

    public CPU()
    {
        A = 0;
        B = 0;
        C = 0;
        D = 0;
        CompareFlag = 0;
        PC = 0;
        console = new Window("CPU log", 400, 300);
    }

    public void shutdown()
    {
        console.dispose();;
    }

    PCB run(PCB proces)
    {
        // set quant of time (priority from 0 to 39):
        int quant = 6 - proces.GetCredit() / 8;

        // return context
        A = proces.getA();
        B = proces.getB();
        C = proces.getC();
        D = proces.getD();
        CompareFlag = proces.getCF();
        PC = proces.getPC();

        if (proces.GetPID() == 1)
        {
            proces.SetStatus(3);
            pr.console.println("PID=" + proces.PID + " change status to running");
            proces.SetStatus(2);
            pr.console.println("PID=" + proces.PID + " change status to ready");
            
        } // INIT process
        else if (proces.GetPID() == 2)
        { // SHELL process
            // exec shell command
            proces.SetStatus(3);
            pr.console.println("PID=" + proces.PID + " change status to running");
            proces.GID = LoginManager.getGid();
            proces.UID = LoginManager.getId();
            shell.scanCommand();
            proces.SetStatus(2);
            pr.console.println("PID=" + proces.PID + " change status to ready");
        } else
        {
            console.println("[CPU] Executing PID=" + proces.GetPID() + " quant=" + quant);
            proces.SetStatus(3);
            pr.console.println("PID=" + proces.PID + " change status to running");
            for (int i = 0; i < quant; i++)
            {
                int j = PC;
                String command = "";
                String arg1 = "";
                String arg2 = "";
                // GET COMMAND:
                char c1 = GetByte(j, proces);
                char c2 = GetByte(++j, proces);
                command = Character.toString(c1) + Character.toString(c2);
                j++;
                if (GetByte(j, proces) == 10)
                {
                    // single command, no arguments
                } else
                {
                    j++;
                    // get arg1
                    while (GetByte(j, proces) != 10 && GetByte(j, proces) != 44)
                    { // dopoki nie ma , lub \n
                        char tmp = GetByte(j, proces);
                        arg1 = arg1 + Character.toString(tmp);
                        j++;
                    }
                    if (GetByte(j, proces) == 10)
                    {    // if endline
                        // single argument arg1
                    } else
                    {
                        j++;
                        while (GetByte(j, proces) != 10)
                        {
                            char tmp = GetByte(j, proces);
                            arg2 = arg2 + Character.toString(tmp);
                            j++;
                        }
                    }
                }
                PC = j;
                //
                // EXEC RECOGNISED COMMAND
                console.println("\tPID=" + proces.GetPID() + "\t" + command + " " + arg1 + "," + arg2);
                if (command.equals("mi"))
                {
                    if (arg1.equals("a"))
                    {
                        A = Integer.valueOf(arg2);
                        console.println("\t\tA=" + A);
                    }
                    if (arg1.equals("b"))
                    {
                        B = Integer.valueOf(arg2);
                        console.println("\t\tB=" + B);
                    }
                    if (arg1.equals("c"))
                    {
                        C = Integer.valueOf(arg2);
                        console.println("\t\tC=" + C);
                    }
                    if (arg1.equals("d"))
                    {
                        D = Integer.valueOf(arg2);
                        console.println("\t\tD=" + D);
                    }
                }
                if (command.equals("mv"))
                {
                    if (arg1.equals("a") && arg2.equals("b"))
                    {
                        A = B;
                    }
                    if (arg1.equals("a") && arg2.equals("c"))
                    {
                        A = C;
                    }
                    if (arg1.equals("a") && arg2.equals("d"))
                    {
                        A = D;
                    }
                    if (arg1.equals("b") && arg2.equals("a"))
                    {
                        B = A;
                    }
                    if (arg1.equals("b") && arg2.equals("c"))
                    {
                        B = C;
                    }
                    if (arg1.equals("b") && arg2.equals("d"))
                    {
                        B = D;
                    }
                    if (arg1.equals("c") && arg2.equals("a"))
                    {
                        C = A;
                    }
                    if (arg1.equals("c") && arg2.equals("b"))
                    {
                        C = B;
                    }
                    if (arg1.equals("c") && arg2.equals("d"))
                    {
                        C = D;
                    }
                    if (arg1.equals("d") && arg2.equals("a"))
                    {
                        D = A;
                    }
                    if (arg1.equals("d") && arg2.equals("b"))
                    {
                        D = B;
                    }
                    if (arg1.equals("d") && arg2.equals("c"))
                    {
                        D = C;
                    }
                }
                if (command.equals("ad"))
                {
                    if (arg1.equals("a") && arg2.equals("b"))
                    {
                        A += B;
                    } else if (arg1.equals("a") && arg2.equals("c"))
                    {
                        A += C;
                    } else if (arg1.equals("a") && arg2.equals("d"))
                    {
                        A += D;
                    } else if (arg1.equals("b") && arg2.equals("a"))
                    {
                        B += A;
                    } else if (arg1.equals("b") && arg2.equals("c"))
                    {
                        B += C;
                    } else if (arg1.equals("b") && arg2.equals("d"))
                    {
                        B += D;
                    } else if (arg1.equals("c") && arg2.equals("a"))
                    {
                        C += A;
                    } else if (arg1.equals("c") && arg2.equals("b"))
                    {
                        C += B;
                    } else if (arg1.equals("c") && arg2.equals("d"))
                    {
                        C += D;
                    } else if (arg1.equals("d") && arg2.equals("a"))
                    {
                        D += A;
                    } else if (arg1.equals("d") && arg2.equals("b"))
                    {
                        D += B;
                    } else if (arg1.equals("d") && arg2.equals("c"))
                    {
                        D += C;
                    } else if (arg1.equals("a"))
                    {
                        A += Integer.valueOf(arg2);
                    } else if (arg1.equals("b"))
                    {
                        B += Integer.valueOf(arg2);
                    } else if (arg1.equals("c"))
                    {
                        C += Integer.valueOf(arg2);
                    } else if (arg1.equals("d"))
                    {
                        D += Integer.valueOf(arg2);
                    }
                }
                if (command.equals("sb"))
                {
                    if (arg1.equals("a") && arg2.equals("b"))
                    {
                        A = A - B;
                    } else if (arg1.equals("a") && arg2.equals("c"))
                    {
                        A = A - C;
                    } else if (arg1.equals("a") && arg2.equals("d"))
                    {
                        A = A - D;
                    } else if (arg1.equals("b") && arg2.equals("a"))
                    {
                        B = B - A;
                    } else if (arg1.equals("b") && arg2.equals("c"))
                    {
                        B = B - C;
                    } else if (arg1.equals("b") && arg2.equals("d"))
                    {
                        B = B - D;
                    } else if (arg1.equals("c") && arg2.equals("a"))
                    {
                        C = C - A;
                    } else if (arg1.equals("c") && arg2.equals("b"))
                    {
                        C = C - B;
                    } else if (arg1.equals("c") && arg2.equals("d"))
                    {
                        C = C - D;
                    } else if (arg1.equals("d") && arg2.equals("a"))
                    {
                        D = D - A;
                    } else if (arg1.equals("d") && arg2.equals("b"))
                    {
                        D = D - B;
                    } else if (arg1.equals("d") && arg2.equals("c"))
                    {
                        D = D - C;
                    } else if (arg1.equals("a"))
                    {
                        A = A - Integer.valueOf(arg2);
                    } else if (arg1.equals("b"))
                    {
                        B = B - Integer.valueOf(arg2);
                    } else if (arg1.equals("c"))
                    {
                        C = C - Integer.valueOf(arg2);
                    } else if (arg1.equals("d"))
                    {
                        D = D - Integer.valueOf(arg2);
                    }
                }
                if (command.equals("ml"))
                {
                    if (arg1.equals("a") && arg2.equals("b"))
                    {
                        A = A * B;
                    } else if (arg1.equals("a") && arg2.equals("c"))
                    {
                        A = A * C;
                    } else if (arg1.equals("a") && arg2.equals("d"))
                    {
                        A = A * D;
                    } else if (arg1.equals("b") && arg2.equals("a"))
                    {
                        B = B * A;
                    } else if (arg1.equals("b") && arg2.equals("c"))
                    {
                        B = B * C;
                    } else if (arg1.equals("b") && arg2.equals("d"))
                    {
                        B = B * D;
                    } else if (arg1.equals("c") && arg2.equals("a"))
                    {
                        C = C * A;
                    } else if (arg1.equals("c") && arg2.equals("b"))
                    {
                        C = C * B;
                    } else if (arg1.equals("c") && arg2.equals("d"))
                    {
                        C = C * D;
                    } else if (arg1.equals("d") && arg2.equals("a"))
                    {
                        D = D * A;
                    } else if (arg1.equals("d") && arg2.equals("b"))
                    {
                        D = D * B;
                    } else if (arg1.equals("d") && arg2.equals("c"))
                    {
                        D = D * C;
                    } else if (arg1.equals("a"))
                    {
                        A = A * Integer.valueOf(arg2);
                    } else if (arg1.equals("b"))
                    {
                        B = B * Integer.valueOf(arg2);
                    } else if (arg1.equals("c"))
                    {
                        C = C * Integer.valueOf(arg2);
                    } else if (arg1.equals("d"))
                    {
                        D = D * Integer.valueOf(arg2);
                    }
                }
                if (command.equals("dv"))
                {
                    if (arg1.equals("a") && arg2.equals("b"))
                    {
                        A = A / B;
                    } else if (arg1.equals("a") && arg2.equals("c"))
                    {
                        A = A / C;
                    } else if (arg1.equals("a") && arg2.equals("d"))
                    {
                        A = A / D;
                    } else if (arg1.equals("b") && arg2.equals("a"))
                    {
                        B = B / A;
                    } else if (arg1.equals("b") && arg2.equals("c"))
                    {
                        B = B / C;
                    } else if (arg1.equals("b") && arg2.equals("d"))
                    {
                        B = B / D;
                    } else if (arg1.equals("c") && arg2.equals("a"))
                    {
                        C = C / A;
                    } else if (arg1.equals("c") && arg2.equals("b"))
                    {
                        C = C / B;
                    } else if (arg1.equals("c") && arg2.equals("d"))
                    {
                        C = C / D;
                    } else if (arg1.equals("d") && arg2.equals("a"))
                    {
                        D = D / A;
                    } else if (arg1.equals("d") && arg2.equals("b"))
                    {
                        D = D / B;
                    } else if (arg1.equals("d") && arg2.equals("c"))
                    {
                        D = D / C;
                    } else if (arg1.equals("a"))
                    {
                        A = A / Integer.valueOf(arg2);
                    } else if (arg1.equals("b"))
                    {
                        B = B / Integer.valueOf(arg2);
                    } else if (arg1.equals("c"))
                    {
                        C = C / Integer.valueOf(arg2);
                    } else if (arg1.equals("d"))
                    {
                        D = D / Integer.valueOf(arg2);
                    }
                }
                if (command.equals("it") && arg1.equals("90"))
                {
                    if (arg2.equals("a"))
                    {
                        System.out.println("PID=" + proces.GetPID() + "\tA=" + A);
                    } else if (arg2.equals("b"))
                    {
                        System.out.println("PID=" + proces.GetPID() + "\tB=" + B);
                    } else if (arg2.equals("c"))
                    {
                        System.out.println("PID=" + proces.GetPID() + "\tC=" + C);
                    } else if (arg2.equals("d"))
                    {
                        System.out.println("PID=" + proces.GetPID() + "\tD=" + D);
                    } else
                    {
                        console.println("error it 90");
                    }
                }
                if (command.equals("it") && arg1.equals("91"))
                {
                    Scanner scanner = new Scanner(System.in);

                    if (arg2.equals("a"))
                    {
                        try
                        {
                            System.out.println("PID=" + proces.PID + "\tA=?");
                            String tekst = scanner.nextLine();
                            A = Integer.parseInt(tekst);
                        } catch (Exception e)
                        {
                            console.println("\terror 91. A=1");
                            A = 1;
                        }
                    } else if (arg2.equals("b"))
                    {
                        try
                        {
                            System.out.println("PID=" + proces.PID + "\tB=?");
                            String tekst = scanner.nextLine();
                            B = Integer.parseInt(tekst);
                        } catch (Exception e)
                        {
                            B = 1;
                            console.println("\terror 91. B=1");
                        }
                    } else if (arg2.equals("c"))
                    {
                        try
                        {
                            System.out.println("PID=" + proces.PID + "\tC=?");
                            String tekst = scanner.nextLine();
                            C = Integer.parseInt(tekst);
                        } catch (Exception e)
                        {
                            C = 1;
                            console.println("\terror 91. C=1");
                        }
                    } else if (arg2.equals("d"))
                    {
                        try
                        {
                            System.out.println("PID=" + proces.PID + "\tD=?");
                            String tekst = scanner.nextLine();
                            D = Integer.parseInt(tekst);
                        } catch (Exception e)
                        {
                            D = 1;
                            console.println("\terror 91. D=1");
                        }
                    } else
                    {
                        System.out.println("error it 91");
                    }
                }
                if (command.equals("it") && arg1.equals("92"))
                {// it 92,xx
                    int start = Integer.parseInt(arg2);
                    int k = start;
                    System.out.print("PID=" + proces.PID + "\t");
                    while (GetByte(k, proces) != 35)
                    {
                        System.out.print(GetByte(k, proces));
                        k++;
                    }
                    System.out.println();
                }
                if (command.equals("cp"))
                {
                    //CompareFlag = 0;              
                    if (arg1.equals("a") && arg2.equals("b"))
                    {
                        if (A == B)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("b") && arg2.equals("c"))
                    {
                        if (B == C)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("a") && arg2.equals("c"))
                    {
                        if (A == C)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("a") && arg2.equals("d"))
                    {
                        if (A == D)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("b") && arg2.equals("a"))
                    {
                        if (B == A)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("b") && arg2.equals("d"))
                    {
                        if (B == D)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("c") && arg2.equals("a"))
                    {
                        if (C == A)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("c") && arg2.equals("b"))
                    {
                        if (C == B)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("c") && arg2.equals("d"))
                    {
                        if (C == D)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("d") && arg2.equals("a"))
                    {
                        if (D == A)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("d") && arg2.equals("b"))
                    {
                        if (D == B)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("d") && arg2.equals("c"))
                    {
                        if (D == C)
                        {
                            CompareFlag = 1;
                        }
                    } else if (arg1.equals("a") && Integer.valueOf(arg2) == A)
                    {
                        CompareFlag = 1;
                    } else if (arg1.equals("b") && Integer.valueOf(arg2) == B)
                    {
                        CompareFlag = 1;
                    } else if (arg1.equals("c") && Integer.valueOf(arg2) == C)
                    {
                        CompareFlag = 1;
                    } else if (arg1.equals("d") && Integer.valueOf(arg2) == D)
                    {
                        CompareFlag = 1;
                    }
                    console.println("\tCompareFlag=" + CompareFlag);
                }
                if (command.equals("ex"))
                {
                    PCB child = pr.fork(proces);
                    pr.exec(child, "/" + arg1, 2 | 4, child.PID, child.UID, child.GID);
                }
                if (command.equals("fw"))
                {
                    //MemoryWrite(int PID, char data, int log_adress, int offset, ArrayList<Page_tab> page_table, FileInfo fi)
                    char datain = arg2.charAt(0);
                    int location = Integer.parseInt(arg1);
                    memory.MemoryWrite(proces.GetPID(), datain, location / 32, location % 32, proces.pageTable, proces.fileInfo);
                }

                if (command.equals("jp"))
                {
                    PC = Integer.valueOf(arg1) - 1;
                    console.println("\tJumping to " + arg1);
                } else if (command.equals("jn") && CompareFlag == 0)
                {
                    PC = Integer.valueOf(arg1) - 1;
                    console.println("\tJumping to " + arg1);
                } else if (command.equals("je") && CompareFlag == 1)
                {
                    PC = Integer.valueOf(arg1) - 1;
                    console.println("\tJumping to " + arg1);
                } else if (GetByte(++j, proces) == 35)
                {
                    console.println("\tDetected # - end of the program");
                    proces.SetStatus(4);
                    pr.console.println("Change process status to completed");
                    break;
                } // DETECT END OF PROGRAM
                //j++;
                PC++;

            }
            //proces.SetStatus(2);
            //pr.console.println("PID=" + proces.PID + " change status to ready");
            if (shell.pause == true)
            {
                try
                {
                    System.out.println("Press enter to continue!");
                    System.in.read();
                } catch (IOException x)
                {
                };
            }
        }
        //try { System.in.read(); } catch (IOException ex) { }
        proces.SetCredit((proces.GetCredit() + 1));
        proces.SetContext(A, B, C, D, CompareFlag, PC);
        return proces;
    }
}
