/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffeetux.Process_Management;

import Console.Window;
import coffeetux.filesystem.*;
import java.util.ArrayList;
import coffeetux.CPU.*;
import coffeetux.memory.Memory;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author Tomasz R.
 */
public class Process_Management
{

    public Window console;
    // process_list = new ArrayList();
    public LinkedList<PCB> process_list;// = new ArrayList<PCB>();
    //ArrayList<Page_tab> pageTable = new ArrayList<Page_tab>();
    public int counter = 2;
    public PCB process;
    public Memory mem = null;
    public CPU cpu;
    public LinkedList<PCB> process_list2;

    int getCounter(int counter)
    {
        return counter;
    }

    public PCB find_process(int pid)
    {
        for (PCB i : process_list)
        {
            if (i.PID == pid)
            {
                return i;
            }
        }
        for (PCB i : process_list2)
        {
            if (i.PID == pid)
            {
                return i;
            }
        }
        return null;
    }

    void is_parrent(int ppid)
    {
        for (PCB i : process_list)
        {
            if (i.PPID == ppid)
            {
                console.println(i.PID + " is child of PID=" + ppid + "\nSetting PPID to 1 (init).");
                i.PPID = 1;
            }

        }
        for (PCB i : process_list2)
        {
            if (i.PPID == ppid)
            {
                console.println(i.PID + " is child of PID=" + ppid + "\nSetting PPID to 1 (init).");
                i.PPID = 1;
            }

        }
    }

    public void printprocesses()
    {
        System.out.println("\nPID \tUID \tGID \tPPID \tstatus \tpriority");
        for (PCB i : process_list)
        {
            System.out.println(i.PID + "\t" + i.UID + "\t" + i.GID + "\t" + i.PPID + "\t" + i.process_status + "\t" + i.priority);
        }
        for (PCB i : process_list2)
        {
            System.out.println(i.PID + "\t" + i.UID + "\t" + i.GID + "\t" + i.PPID + "\t" + i.process_status + "\t" + i.priority);
        }
        System.out.println();
    }

    public int status_tab[] = new int[5];

    static
    {
        int status_tab[] =
        {
            1, 2, 3, 4
        };
        /*
         1-new
         2-ready
         3-work
         4-zombie
         */
    }

    /**
     * @param args the command line arguments
     */
    public void main(String[] args)
    {
        PCB init = new PCB();
        init.PID = 1;
        init.PPID = -1;
        init.UID = -1;
        init.GID = -1;
        init.fileInfo = null;
        init.credit = -1;
        init.process_status = 3;
        init.priority = 39;
        process_list.add(init);
    }

    public Process_Management()
    {
        console = new Window("PrManag log", 400, 300);
    }

    public void shutdown()
    {
        console.dispose();
    }

    public PCB fork(PCB parent)
    {
        console.println("Creating new process.");
        PCB process = new PCB();
        counter++;
        console.println("New PID=" + counter);
        process.PID = counter;
        process.PPID = parent.PID;
        process.fileInfo = parent.fileInfo;
        process.credit = parent.credit;
        console.println("Process status = 'new'");
        process.process_status = status_tab[1];
        //process.priority = parent.priority;
        int tmp;
        Random r = new Random();
        tmp = r.nextInt(31) + 5;
        process.priority = tmp;
        process.pageTable = parent.pageTable;
        process_list.add(process);
        System.out.println("Creating new process. PID=" + process.PID);
        return process;
    }

    public void kill(int pid)
    {
        console.println("Killing process PID=" + pid);
        System.out.println("Killing process PID=" + pid);
        //PCB process = (PCB) getProcess(pid);
        PCB process = find_process(pid);
        if (process != null)
        {
            ArrayList<Page_tab> temp = process.pageTable;
            is_parrent(pid);
            process_list.remove(process);
            process_list2.remove(process);
            mem.SaveProcess(pid, temp, process.fileInfo);
            FileSystem.Close(process.fileInfo);
        }
        else
        {
            System.out.println("ERROR: Process PID=" + pid + " not found.");
        }
    }

    public void exit(int pid)
    {
        process = (PCB) getProcess(pid);
        if (process.process_status == 4);
        {
            console.println("Exiting process PID=" + pid);
            ArrayList<Page_tab> temp = process.pageTable;
            is_parrent(pid);
            process_list.remove(process);
            process_list2.remove(process);
            mem.SaveProcess(process.PID, temp, process.fileInfo);
            FileSystem.Close(process.fileInfo);
        }
    }

    public void exec(PCB process, String filepath, int flags, int pid, int uid, int gid)
    {
        process.GID = gid;
        process.UID = uid;
        try
        {
            process.fileInfo = FileSystem.Open(filepath, flags, pid);
            int length = FileSystem.GetFileSize(process.fileInfo);
            process.pageTable = new ArrayList();
            for (int i = 0; i < Math.ceil(length / 32f); i++)
            {
                Page_tab pg = new Page_tab(i);
                process.pageTable.add(pg);
            }
        }
        catch (FileSystemException ex)
        {
            // to sie nie powinno zdarzyc bo wczesniej shell rzuci wyjatek
        }
        mem.console.println("PID: " + process.PID + " -> " + process.pageTable.size() + " pages created");
    }

    public int getUID(int pid)
    {
        for (int i = 0; i < process_list.size(); i++)
        {
            if (process_list.get(i).PID == pid)
            {
                return process_list.get(i).UID;
            }
        }
        return -1;
    }

    public int getGID(int pid)
    {
        for (int i = 0; i < process_list.size(); i++)
        {
            if (process_list.get(i).PID == pid)
            {
                return process_list.get(i).GID;
            }
        }
        return -1;
    }

    public PCB getProcess(int pid)
    {
        for (int i = 0; i < process_list.size(); i++)
        {
            if (process_list.get(i).PID == pid)
            {
                return process_list.get(i);
            }
        }

        return null;
    }

    public Page_tab getPage(int pid, int page_number)
    {
        for (int i = 0; i < process_list.size(); i++)
        {
            if (process_list.get(i).PID == pid)
            {
                return process_list.get(i).pageTable.get(page_number);
            }
        }

        for (int i = 0; i < process_list2.size(); i++)
        {
            if (process_list2.get(i).PID == pid)
            {
                return process_list2.get(i).pageTable.get(page_number);
            }
        }

        return null;
    }
}

