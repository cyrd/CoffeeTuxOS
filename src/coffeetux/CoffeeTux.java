/*

 {     }     }   {     }                    Poznan University of Technology
 }___{_____{_____}___{                                  2014
 .-'{    }     }   {    }'-.              
 (    }        {        {    )                  ####  ####  #### #### #### ####
 |'-..___________________..-'|                 #     #    # #    #    #    #
 |           a8888b.         |                 #     #    # ###  ###  ###  ###
 |          8P"YP"Y88        ;-----.           #     #    # #    #    #    #
 |          8|o||o|88       (____    \          ####  ####  #    #    #### ####
 |          8'    .88        |    \   \             
 |          8`._.' Y8.       |     \   \     ####### #   # #   #     ####   ####      
 |         d/      `8b.      |      )   )       #    #   #  # #     #    # #
 |       .dP   .     Y8b.    |     /   /        #    #   #   #      #    #  ###
 |      d8:'   "   `::88b.   |    /   /         #    #   #  # #     #    #     #
 |     d8"           `Y88b   |   /   /          #    ##### #   #     ####  #### 
 |    :8P     '       :888   |  /   /
 |     8a.    :      _a88P   | /   /         Academic Operating System Simulation
 |   ._/"Yaa_ :    .| 88P|   |/   /                  in Java Environment          
 |   \    YP"      `| 8P  `. /   /                    
 |   /     \._____.d|    .' (  ,'                        written by:
 \  `--..__)888888P`._.'    y'  S.Talarczyk(CPU) P.Sendrowski(Memory) S.Szneider(FileSystem)
 '-..__________________..-'  T.Rozmacinski(ProcessManagement) M.Werda(Users) M.Kmieciak(I/O)

 */
package coffeetux;

import java.lang.String;
import coffeetux.CPU.*;
import coffeetux.Process_Management.Process_Management;
import coffeetux.Shell.Shell;
import coffeetux.memory.*;
import java.util.LinkedList;
import coffeetux.filesystem.FileSystem;

public class CoffeeTux
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            System.out.print(goodbye + "\n\nWelcome in CoffeeTux. Please wait awhile while system is loading...\n\n\n");
            Thread.sleep(1000);                 //1000 milliseconds is one second.
        } catch (InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }

//CPU structures
        CPU procesor = new CPU();
        scheduler planista = new scheduler();
        LinkedList<PCB> ReadyProcess = new LinkedList<PCB>();
        LinkedList<PCB> SuspendedProcess = new LinkedList<PCB>();

        //filesystem structures
        //? wszystkie potrzebne struktury do obslugi dysku
        FileSystem disk = new FileSystem();
        Process_Management processes = new Process_Management();

        //memory structures
        //? wszystkie potrzebne struktury do obslugi pamieci wirtualnej
        Memory memory = new Memory();

        //comunication structures;
        FileSystem.pm = processes;
        memory.processes = processes;
        procesor.pr = processes;
        procesor.memory = memory;
        planista.processes = processes;
        processes.mem = memory;
        procesor.shell = new Shell();
        FileSystem.dc = procesor.shell.devices;
        procesor.shell.fileSystem = disk;
        procesor.shell.pm = processes;
        procesor.shell.mem = memory;
        processes.cpu = procesor;
        processes.process_list = ReadyProcess;
//initialization of first process (init and shell)
        ReadyProcess.add(new PCB(1, 39)); // INIT process
        ReadyProcess.add(new PCB(2, 5));   // SHELL process
        procesor.shell.usersManager.getGroups();
        procesor.shell.usersManager.startUser();
        procesor.shell.startFileSystem();
        processes.process_list2 = SuspendedProcess;
        procesor.shell.io = procesor.shell.devices.makedefault();
        procesor.shell.devices.loaddevs();
        procesor.shell.disk = procesor.shell.devices.makedefaultst();
        
        //main operating system loop
        String x[] =
        {
            "mi c,40\n" + "mi d,95\n" + "ad d,c\n" + "it 90,d\n" + "#", //p0
            
            "it 91,b\n"+ //p1
            "it 91,c\n"+
            "mv d,b\n"+
            "cp c,0\n"+
            "je 79\n"+
            "cp c,1\n"+
            "je 69\n"+
            "sb c,1\n"+
            "ml b,d\n"+
            "jp 36\n"+
            "it 90,b\n"+
            "#\n"+
            "mi a,1\n"+
            "it 90,a\n"+
            "#",           
            
            "it 91,a\n"+ //p2
            "mv c,a\n"+
            "mi b,1\n"+
            "ml a,b\n"+
            "ad b,1\n"+
            "cp b,c\n"+
            "jn 22\n"+
            "mv d,a\n"+
            "it 90,d\n"+
            "#",
            "mi a,5\n"+ //p3
            "jp 0\n#",
            
            "ex p3\n"+ //p4
            "mi a,5\n"+
            "jp 6\n"+
            "#",
            
            "it 91,a\n"+ //p5
            "it 91,b\n"+
            "ad a,b\n"+
            "dv a,2\n"+
            "it 90,a\n"+
            "#",
            
            "fw 79,H\n"+ //p6
            "it 92,79\n"+
            "fw 79,E\n"+
            "it 92,79\n"+
            "fw 79,L\n"+
            "it 92,79\n"+
            "it 92,79\n"+
            "fw 79,O\n"+
            "it 92,79\n"+
            "#\n"+
            "H\n"+
            "#",
                
            "mi a,30\n"+ //p7 program odlicza od 30 do 0
            "it 90,a\n"+
            "sb a,1\n"+
            "cp a,0\n"+
            "jn 08\n"+
            "it 90,a\n"+
            "#"
        };
        FileSystem.SaveFromStrings(x);

        while (!procesor.shell.exit)
        {
            planista.take(ReadyProcess, SuspendedProcess, procesor);
        }

        //closing methods
        FileSystem.shutdown();
        memory.Shutdown();
        procesor.shutdown();
        processes.shutdown();

        System.out.println("[:)] Thank you for using CoffeeTux, academical project of operating system!");
        System.out.println("\n\n" + goodbye + "\n");
        try
        {
            Thread.sleep(1500);
        } catch (Exception e)
        {

        }

    }
    static String goodbye = "     {     }     }   {     }                    Poznan University of Technology\n"
            + "      }___{_____{_____}___{                                  2014\n"
            + "   .-'{    }     }   {    }'-.              \n"
            + "  (    }        {        {    )                  ####  ####  #### #### #### ####\n"
            + "  |'-..___________________..-'|                 #     #    # #    #    #    #\n"
            + "  |           a8888b.         |                 #     #    # ###  ###  ###  ###\n"
            + "  |          8P\"YP\"Y88        ;-----.           #     #    # #    #    #    #\n"
            + "  |          8|o||o|88       (____    \\          ####  ####  #    #    #### ####\n"
            + "  |          8'    .88        |    \\   \\             \n"
            + "  |          8`._.' Y8.       |     \\   \\     ####### #   # #   #     ####   ####      \n"
            + "  |         d/      `8b.      |      )   )       #    #   #  # #     #    # #\n"
            + "  |       .dP   .     Y8b.    |     /   /        #    #   #   #      #    #  ###\n"
            + "  |      d8:'   \"   `::88b.   |    /   /         #    #   #  # #     #    #     #\n"
            + "  |     d8\"           `Y88b   |   /   /          #    ##### #   #     ####  #### \n"
            + "  |    :8P     '       :888   |  /   /\n"
            + "  |     8a.    :      _a88P   | /   /         Academic Operating System Simulation\n"
            + "  |   ._/\"Yaa_ :    .| 88P|   |/   /                  in Java Environment          \n"
            + "  |   \\    YP\"      `| 8P  `. /   /                    \n"
            + "  |   /     \\._____.d|    .' (  ,'                        written by:\n"
            + "   \\  `--..__)888888P`._.'    y'  S.Talarczyk(CPU) P.Sendrowski(Memory) S.Szneider(FileSystem)\n"
            + "    '-..__________________..-'  T.Rozmacinski(ProcessManagement) M.Werda(Users) M.Kmieciak(I/O)";

}
