/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffeetux.io;
//import coffeetux.filesystem.*;
import coffeetux.filesystem.FileSystemException;
import coffeetux.filesystem.FileSystem;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
/**
 *
 * @author Martin
 */
public class iodevice extends device {
    boolean busy;
    String in;
    String buffer;
    String output;
    String filename;
    public boolean def;
   public iodevice()
    {
       name="Default IO device";
       type=1;
       in="";
       output="";
       busy=false;
       filename="D:\\iodevice.txt";
        def=false;

       try
        {
            File file = new File(filename);  
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(name+"\n");
            writer.write(type+"\n");
            writer.write(in+"\n");
            writer.write(output+"\n");
            writer.write(filename);
            writer.close();
    //        FileSystem.Open("/iodevice",FileSystem.F_CREATE,0,700);
            
        }
        catch(IOException e)
           {
           System.out.print(e);
           }

    }
       public iodevice(String name)
       {
       this.name=name;
       filename=name+".txt";
       type=1;
       in="";
       output="";
       busy=false;
       def=false;
       try
        {
            
            File file = new File(this.filename);  
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(this.name+"\n");
            writer.write(type+"\n");
            writer.write(in+"\n");
            writer.write(output+"\n");
            writer.write(filename);
            writer.close();
        }
        catch(Exception e)
           {
           System.out.print(e);
           }
        
        System.out.println("Created io device: "+name);
    }
       public String getin()
       {
           return in;
       }
       public String getout()
       {
           return output;
       }
       public void setin(String data){
           in=data;
           saveio();
       }
       public void setout(String data){
           output=data;
           saveio();
       }
       
       public void saveio()
       {
           try
        {
            File file = new File(filename);  
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(name+"\n"+type+"\n"+in+"\n"+output+"\n"+filename);
            writer.close();
        }
        catch(IOException e)
           {
           System.out.print(e);
           }
       
       }
        public void saveio(String filename)
       {
           try
        {
            File file = new File(filename);  
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(name+"\n"+type+"\n"+in+"\n"+output+"\n"+filename);
            writer.close();
        }
        catch(IOException e)
           {
           System.out.print(e);
           }
        
       }
       public void readio(String filename)
       {
           try
        {
            String tempname;
            int temptype;
            File file = new File(filename);  
            BufferedReader reader = new BufferedReader(new FileReader(file));
            tempname=reader.readLine();
            temptype=Integer.parseInt(reader.readLine());
            if(temptype!=1) {
                System.out.print("\nIncorrect type of device, accepted only type 1(io devices)\n");
            }
            else{
            name=tempname;
            type=temptype;
            in=reader.readLine();
            output=reader.readLine();
            this.filename=reader.readLine();
            }
            reader.close();
        }
        catch(IOException e)
           {
           System.out.print(e);
           }
        
       }
       
       public void readio()
       {
           try
        {
            String tempname;
            int temptype;
            File file = new File(filename);  
            BufferedReader reader = new BufferedReader(new FileReader(file));
            tempname=reader.readLine();
            temptype=Integer.parseInt(reader.readLine());
            if(temptype!=1) {
                System.out.print("\nIncorrect type of device, accepted only type 1(io devices)\n");
            }
            else{
            name=tempname;
            type=temptype;
            in=reader.readLine();
            output=reader.readLine();
            filename=reader.readLine();
            }
            reader.close();
        }
        catch(IOException e)
           {
           System.out.print(e);
           }
          
       }
       
       
       public void handling()
       {
        char clear=' ';
        if(busy==true)
        {
            System.out.println(name+"is busy");
            while(busy==true) System.out.println("Waiting for device: "+name);
            busy=false;
        }
        
        if(busy==false)
        {
            if(output!="")
                {
                busy=true;
                write();
                clear='o';
                busy=false;
                }
            
            else if(in!="")
                {
                busy=true;
                buffer=read();
                clear='i';
                busy=false;
                }
            }
        if(clear=='o')
        {
            setout("");
        }
        if(clear=='o')
        {
            setin("");
        }
        clear=' ';
         
        }
    
    
    public String read()
    {
       return in; 
    }
    public void write()
    {
       System.out.print(output);
    }
    
    public void cout(String data)
    {
    setout(data);
    handling();
    }
    
    public String get()
    {
        Scanner scan = new Scanner(System.in);
        String input;
        input=scan.nextLine();
        return input;
    }
    
    
    public String cin()
    {  
    System.out.println();
    cout("Please type input stream: ");  
    System.out.println();
    setin(get());
    handling();
    return buffer;
    }
    
    public String cin(String data)
    {
    setin(data);
    handling();
    return buffer;
    }
    public void removefile()
    {
        File dev=new File(this.filename);
        dev.delete();
    }
    
    public void show()
    {
        System.out.print("\nDevice name: "+name+", type:"+type+", busy:"+busy+", input data:"+in+", output data: "+output+", filepath:"+filename+"\n");
    }
    
}
