/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffeetux.io;
import coffeetux.filesystem.FileSystem;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.File;
/**
 *
 * @author Martin
 */
public class storagedevice extends device {
    boolean busy;
    public boolean def;
    String device_filename;
    String data_filename;
    char[][] memory=new char[32][16];

    public storagedevice()
    {
        name="Removable disk";
        type=2;
        device_filename="D:\\disk-device.txt";
        data_filename="D:\\disk-data.txt";
        def=false;
        try
        {
            File file = new File(device_filename);  
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(name+"\n");
            writer.write(type+"\n");
            writer.write(data_filename);
            writer.close();
  
        }
        catch(IOException e)
           {
           System.out.print(e);
           }
    }
    public storagedevice(String name)
    {

        this.name=name;
        type=2;
        device_filename=name+".txt";
        data_filename=name+"-data.txt";
        def=false;
        try
        {
            File file = new File(device_filename);  
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(this.name+"\n");
            writer.write(type+"\n");
            writer.write(data_filename);
            writer.close();
        }
        catch(IOException e)
           {
           System.out.print(e);
           }
    }
    
    public void usage(iodevice temp)
    {
        readmemory(temp);
    
        System.out.println();
          print("Usage of "+name+":",temp);
          System.out.println();
        for(int i=0;i<32;i++)
        {
            for(int j=0;j<16;j++)
            {               
                print("["+i+","+j+"] = "+memory[i][j]+",",temp);
                if(j==15) System.out.println();
            }
        }
   
        
    }
    public void writememorycell(int x, int y, char data,iodevice temp){
        
        if(x>=0 && x<32 && y>=0 && y<16)
        {
            System.out.println("Writing data to cell: ["+x+","+y+"], of device: "+name);
            memory[x][y]=data;
        }else
        {
                System.out.println();
            print("Bad memory cell - [max = 32][max = 16]",temp);
                System.out.println();
        }
        savememory(temp);

    }
    public void readmemorycell(int x, int y,iodevice temp){
        char data='!';
        String ret_value="";
        if(x>=0 && x<32 && y>=0 && y<16)
        {
            System.out.println("Reading data from cell: ["+x+","+y+"], of device: "+name);
            data=memory[x][y];
            ret_value=ret_value+data;
            
                System.out.println();
        }else temp.cout("Bad memory cell - [max = 32][max = 16]");
         System.out.println();
        temp.cout(ret_value);
         System.out.println();
    }
    public void writememoryfromstart(iodevice temp)
    {
       
        String stream="";
        int x=0;
        int j=0;
        print("Writing memory from block nr 0 to end of stream, to device: "+name,temp);
            System.out.println();
        print("Type data to write to memory - max 512 characters: ",temp);
            System.out.println();
        stream=read(temp);
         int lenght=stream.length();
        if(lenght<512)
        {
        int blocks=stream.length()/32;
        if(stream.length()<=32)
        {
            for(int i=0; i<stream.length(); i++)
            {
                memory[i][j]=stream.charAt(i);
            }
        }
        else
        {
          for(int y=0; y<blocks; y++)
          {
            for(int i=0; i<32; i++)
            {
                memory[i][j]=stream.charAt(x);
                if(i==31) j++;
                x++;
                lenght--;
            }
            if(lenght<=32)
            {
            for(int i=0; i<stream.length() % 32; i++)
            {
                memory[i][j]=stream.charAt(x);
                if(i==31) j++;
                x++;
                lenght--;
            } 
            }
          }
        } 
        } else print("Too long stream,max=512",temp);
        System.out.println();
        print("Successfully writed data to: "+name,temp);
        System.out.println();
        savememory(temp);        
    }
    
    public void writememoryfromcell(int i, String data, iodevice temp)
    {

        int a=0;
        int b=0;
        
        for(int z=0; z<i; z++)
        {
            a++;
            if(a==32)
            {
                a=0;
                b++;
            }
                
        }

        for(int x=0; x<data.length(); x++)
        {
            memory[a][b]=data.charAt(x);
            a++;
            if(a==31)
            {
                a=0;
                b++;
            }
            if(b>15) temp.cout("Too low memory to save, change the memory cell");
        }
        savememory(temp);

    }
    public String readmemoryfromcell(int i, int lenght, iodevice temp)
    {
        int a=0;
        int b=0;
        String data="";
        for(int z=0; z<i; z++)
        {
            a++;
            if(a==32)
            {
                a=0;
                b++;
            }
                
        }

        for(int x=0; x<lenght; x++)
        {
            data+=memory[a][b];
            a++;
            if(a==31)
            {
                a=0;
                b++;
            }
            if(b>15) temp.cout("End of memory");
        }
        savememory(temp);
        return data;
    }
    public void readmemoryfromcell2(int i, int lenght, iodevice temp)
    {
        int a=0;
        int b=0;
        String data="";
        for(int z=0; z<i; z++)
        {
            a++;
            if(a==32)
            {
                a=0;
                b++;
            }
                
        }

        for(int x=0; x<lenght; x++)
        {
            data+=memory[a][b];
            a++;
            if(a==31)
            {
                a=0;
                b++;
            }
            if(b>15) temp.cout("End of memory");
        }
        savememory(temp);
        System.out.println();
        temp.cout(data);
         System.out.println();
    }
    
    public void writeblock(int block_nr,iodevice temp){
        String stream="";
            System.out.println();
        print("Type data to write to block nr: "+block_nr+", max 32 chars. Device: "+name,temp);
        System.out.println();
        stream=read(temp);
        if(stream.length()<=32){
        for(int i=0;i<stream.length();i++){
            memory[i][block_nr]=stream.charAt(i);
        }
        for(int i=stream.length();i<32;i++)
        {
          memory[i][block_nr]=' '; 
        }
        System.out.println();
        print("Successfully writed data to block: "+block_nr+", device: "+name,temp);
        System.out.println();
        savememory(temp);

        } else
        {
   
            System.out.println();
        temp.cout("Too long stream max=32 chars!");
            System.out.println();
        }
         
        
    }
   public void readblock(int block_nr, iodevice temp){    
        System.out.println();
        print("Reading data from block: "+block_nr+", of device: "+name,temp);
        System.out.println();
        String stream="";
        char tempchar;
        for(int i=0;i<32;i++){ 
            tempchar=memory[i][block_nr];
            stream=stream+tempchar;
         }
        System.out.println();
        print("Successfully readed data from block: "+block_nr+", of device: "+name,temp);
        System.out.println();
        temp.cout(stream);
        System.out.println();
    }
    public void savememory(iodevice temp)
    {

        try{
            System.out.println();
            print("Saving memory to file: "+data_filename,temp);
            System.out.println();
            makefiles(temp);
            File file = new File(data_filename);  
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            
            for(int i=0;i<32;i++)
            {
            for(int j=0;j<16;j++)
            {       
                writer.write(memory[i][j]); 
            }
            }
            writer.close();
            System.out.println();
            print("Successfully saved memory to: "+data_filename,temp);
            System.out.println();
        }
        catch(IOException e)
           {
           System.out.print(e);
           }

}
    
   public void readmemory(iodevice temp)
    {
        try
        {
            System.out.println();
            print("Reading memory from device: "+name,temp);
            System.out.println();
            makefiles(temp);
            File file = new File(data_filename);  
            FileReader reader = new FileReader(file);
            int r;
            for(int i=0;i<32;i++)
            {
            for(int j=0;j<16;j++)
            {    
            if ((r=reader.read()) != -1) 
            {
            char ch = (char) r;
            if(ch!=' ')
            memory[i][j]=ch;
            }
            }
            }
            reader.close();
            System.out.println();
            print("Successfully read memory from device: "+name,temp);
            System.out.println();
             savememory(temp);
        }
        catch(IOException e)
           {
           System.out.print(e);
           }
       
    }
    public void readmemory(String data_filename, iodevice temp)
    {
        try
        {
            System.out.println();
            print("Reading memory from: "+data_filename,temp);
            System.out.println();
            makefiles(temp);
            File file = new File(data_filename);  
            FileReader reader = new FileReader(file);
            int r;
            for(int i=0;i<32;i++)
            {
            for(int j=0;j<16;j++)
            {    
            if ((r=reader.read()) != -1) 
            {
            char ch = (char) r;
            if(ch!=' ')
            memory[i][j]=ch;
            }
            }
            }
            reader.close();
            System.out.println();
        print("Successfully read memory",temp);
        System.out.println();
        savememory(temp);
        }
        catch(IOException e)
           {
           System.out.print(e);
           }
        
        
    }
    public void clearmemory(iodevice temp)
    {
        System.out.println();
            print("Clearing memory",temp);
            System.out.println();
        for(int i=0;i<32;i++)
            {
            for(int j=0;j<16;j++)
            {  
                memory[i][j]=' ';
            }
            }
            System.out.println();
            print("Successfully cleared memory",temp);
            savememory(temp);
            System.out.println();
    }
    public void print(String data, iodevice temp){
    temp.cout(data);
    }
    public String read(iodevice temp){
    return temp.cin();
    }
    public String read(String data,iodevice temp){
    return temp.cin(data);
    }
    public void readdevice(String filename, iodevice temp){
        try
        {
         System.out.println();
            print("Reading device from: "+filename,temp);
            System.out.println();   
            String tempname;
            int temptype;
            File file = new File(filename);  
            BufferedReader reader = new BufferedReader(new FileReader(file));
            tempname=reader.readLine();
            temptype=Integer.parseInt(reader.readLine());
            if(temptype!=2) {
                print("Incorrect type of device, accepted only type 2(storage devices)",temp);
                System.out.println();
            }
            else{
            name=tempname;
            type=temptype;
            data_filename=reader.readLine();
            device_filename=filename;
            readmemory(temp);
            System.out.println();
            print("Successfully read device from: "+filename,temp);
            System.out.println();
            }
            reader.close();
        }
        catch(IOException e)
           {
           System.out.print(e);
           }
    }
    public void show(iodevice temp)
    {
        System.out.println();
        print("Device name: "+name+", type:"+type+", data file path: "+data_filename,temp);
        System.out.println();
    }
    public void showusingfiles(iodevice temp)
    {
        System.out.println("Files using by: "+name);
        System.out.println("----------------------------");
        System.out.println("Storage device: "+device_filename);
        System.out.println("Storage data: "+data_filename);
        System.out.println("Storage device: "+temp.filename);
        System.out.println("----------------------------");
    }
    public void deletefiles(iodevice temp)
    {
        System.out.println();
            print("Deleting files",temp);
            System.out.println();
        File dev=new File(device_filename);
        File dat=new File(data_filename);
        dev.delete();
        dat.delete();
        System.out.println();
            print("Succcessfully deleted files",temp);
            System.out.println();
    }
    public void makefiles(iodevice temp)
    {

            File dev = new File(this.device_filename);
            File data = new File(this.data_filename);
            if(!dev.exists())
            {
            try
            { 
            System.out.println();
            System.out.println();
            dev.createNewFile();
            FileWriter writer = new FileWriter(dev);
            writer.write(this.name+"\n");
            writer.write(this.type+"\n");
            writer.write(this.data_filename);
            writer.close();
            }
            catch(IOException e)
            {
            System.out.print(e);
            }    
            }
            if(!data.exists())
            {
            try
            { 

            data.createNewFile();
            }
            catch(IOException e)
            {
            System.out.print(e);
            }    
            }
            
        }

    }
