/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffeetux.io;
import java.util.LinkedList;
import Console.Window;
import coffeetux.filesystem.FileInfo;
import coffeetux.filesystem.FileSystem;
import java.util.ArrayList;
/**
 *
 * @author Martin
 */
public class devicecollector {
    public LinkedList <iodevice> iodevs = new <iodevice>  LinkedList();
    public LinkedList <storagedevice> storagedevs = new <storagedevice>  LinkedList();
    public static Window iolog = new Window("IO log", 400, 300);
    
    public void addio(iodevice temp)
    {
        iodevs.add(temp);
    }
    public void addstorage(storagedevice temp)
    {
        storagedevs.add(temp);
    }
    public void showdevices()    
    {
        System.out.println("==========================");
                System.out.println("\nIO - devices\n");
         if(iodevs.size()==0) System.out.println("You don't have any iodevice");
        else
        {
        System.out.println("Id  -   Name    -  Device filepath  -   Default device");
        for(int i=0; i<iodevs.size(); i++)
        {
        System.out.println(i+"  -   "+iodevs.get(i).name+"  -   "+iodevs.get(i).filename+"  -   "+iodevs.get(i).def);
        }
        }
        
         System.out.println("\nStorage - devices\n");
        if(storagedevs.size()==0) System.out.println("You don't have any storagedevice");
        else
        {
        
        System.out.println("Id  -   Name    -  Device filepath  -   Datafilepath");
        for(int i=0; i<storagedevs.size(); i++)
        {
        System.out.println(i+"   -   "+storagedevs.get(i).name+"  -   "+storagedevs.get(i).device_filename+"  -   "+"  -   "+storagedevs.get(i).data_filename);
        }
        }
        System.out.println("\n==========================");
    }
    public void showio()    
    {
        System.out.println("==========================");
                System.out.println("\nIO - devices\n");
         if(iodevs.size()==0) System.out.println("You don't have any iodevice");
        else
        {
        System.out.println("Id\t\tName\t\tDevice filepath\t\tDefault device");
        for(int i=0; i<iodevs.size(); i++)
        {
        System.out.println(i+"\t\t"+iodevs.get(i).name+"\t\t"+iodevs.get(i).filename+"\t\t"+iodevs.get(i).def);
        }
        }

        System.out.println("\n==========================");
    }
     public void showst()    
    {
        System.out.println("==========================");
        
         System.out.println("\nStorage - devices\n");
        if(storagedevs.size()==0) System.out.println("You don't have any storagedevice");
        else
        {
        
        System.out.println("Id\t\tName\t\tDevice filepath\t\tData filepath\t\tDefault");
        for(int i=0; i<storagedevs.size(); i++)
        {
        System.out.println(i+"\t\t"+storagedevs.get(i).name+"\t\t"+storagedevs.get(i).device_filename+" \t\t"+storagedevs.get(i).data_filename+"\t\t"+storagedevs.get(i).def);
        }
        }
        System.out.println("\n==========================");
    }
    
    

     
    public void write(String name, String data, int i)
    {
        iodevice temp=null;
        for(int x=0; x<iodevs.size(); x++)
        {
            if(iodevs.get(x).def==true)
                temp=iodevs.get(x);
        }
        for(int j=0; j<storagedevs.size();j++)
        {
        if(storagedevs.get(j).name.equals(name))
        {
            storagedevs.get(j).writememoryfromcell(i, data, temp);
        }
        }
            
    }
     public String read(String name, int length, int i)
    {
        iodevice temp=null;
        for(int x=0; x<iodevs.size(); x++)
        {
            if(iodevs.get(x).def==true)
                temp=iodevs.get(x);
        }
        for(int j=0; j<storagedevs.size();j++)
        {
        if(storagedevs.get(j).name.equals(name))
        {
          return  storagedevs.get(j).readmemoryfromcell(i, length, temp);
        }
        }
         return "";   
    }
     
     public void shutdown()
     {
         iolog.dispose();
     }
     
     public void loaddevs()
     {
         try
         {
      FileInfo tmp = FileSystem.Open("/mnt/",FileSystem.F_READ,1);
      ArrayList<String[]> list;
      list = FileSystem.ListDir(tmp);
      FileSystem.Close(tmp);
      if(list.isEmpty()==false)
      {
      storagedevice tempstorage;
      iodevice tempio=new iodevice();
      for(int i=0; i<list.size(); i++)
      {
         tempstorage=new storagedevice(list.get(i)[0]);
         tempstorage.readdevice(tempstorage.name+".txt",tempio);
         storagedevs.add(tempstorage);  
      }
      storagedevs.getFirst().def=true;
      }
      else iolog.println("Any devices to read");
         }
         catch(Exception e)
                 {
                     iolog.println(e);
                 }
     }
     
     public iodevice makedefault()
     {
         try
         {
         FileSystem.CreateDir("/mnt/", 1, 777);
         iodevice temp;
         temp = new iodevice("Defaultio");
         this.iodevs.addFirst(temp);
         temp.def=true;
         return temp;
         }
         catch (Exception e)
         {
             iolog.println(e);
         }
         return null;
     }
     
     public storagedevice makedefaultst()
     {
         try
         {
         if(storagedevs.isEmpty()==true)
         {
         storagedevice temp;
         String name = "Defaultstorage";
         temp = new storagedevice(name);
         this.storagedevs.addFirst(temp);
         temp.def=true;
         FileSystem.CreateFile("/mnt/"+name,1,777);
         FileInfo fi = FileSystem.Open("/mnt/"+name, FileSystem.F_WRITE | FileSystem.F_READ, 1);
         FileSystem.SetFileType(fi, 4);
         FileSystem.Close(fi);
         return temp;
         }
         else return storagedevs.getFirst();
         } catch(Exception e)
         {
             iolog.println(e);
         }
         return null;
     }
     
     public void removedevs()
    {
    for (int i = 0; i <iodevs.size(); i++)
                        {
                            iodevs.get(i).removefile();
                        }
                            iodevs.clear();
    }
   
              
}
