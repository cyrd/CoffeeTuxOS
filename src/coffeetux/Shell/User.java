package coffeetux.Shell;

import java.util.*;
import java.lang.Iterable;

public class User 
{
    int id;
    int gid;
    String name;
    String hash;
    
    User()
    {
        this.gid = -1;
        this.id = -1;
        this.name = "INVALID";
    }
    
    User(String name)
    {
        this.name = name;
        this.gid = -1;
        this.id = -1;
    }
    
    User(String name, int gid)
    {
        this.name = name;
        this.gid = gid;
    }
    
    User(String name, int id, int gid)
    {
        this.name = name;
        this.id = id;
        this.gid = gid;
    }
    
        User(String name, int id, int gid, String hash)
    {
        this.name = name;
        this.id = id;
        this.gid = gid;
        this.hash = hash;
    }
}
