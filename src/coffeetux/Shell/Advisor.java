package coffeetux.Shell;

public class Advisor
{

    public static int size = 70;
    public static String[] advice;

    Advisor()
    {
        advice = new String[this.size];
        int i = 0;
        advice[i++] = "login - allows you to log in or relog as a user with specified name. Logs out from the current account";
        advice[i++] = "logout - logs out form user's account and only allows to relog.";
        advice[i++] = "adduser - allows you to create a new user to the default group. Only root can add users.";
        advice[i++] = "users [oPARAM] - views the list of all users registered in the operating system. Without any parameter it prints names, id's and gid's. Optional parameters \n"
                      + "are: [names] to list only names, [id] to view the list of id's and [gid] for the series of gid's with duplicates.";
        advice[i++] = "groups - prints formatted list of all groups and user that are present in the operating system.";
        advice[i++] = "whoami - prints crucial informations about the logged user.";
        advice[i++] = "addgroup - opens up an interface to create a new group of users.";
        advice[i++] = "moveuser - opens up an interface to change user's group.";
        advice[i++] = "userdel - opens up an interface allowing you to delete single user.";
        advice[i++] = "groupdel - opens up an interface allowing you to delete the whole group of users, but with moving the users to the default group.";
        advice[i++] = "chuname - change actual user's name";
        advice[i++] = "chgname - change actual user's group name";
        advice[i++] = "password - change actual user's name";

        advice[i++] = "kill [PARAM] - kills process with PID given as parameter";
        advice[i++] = "ps - prints list of processes";

        advice[i++] = "editor - opens up an editor to create or modify a file";

        advice[i++] = "lds - builds a list of files in the folder you actually browse.";
        advice[i++] = "exec - a command used to execute a file.";
        advice[i++] = "cd [PARAM] - changes folder you actually browse. As the parameter, type the name of the folder to get inside of it or \"..\" to go one folder back.";
        advice[i++] = "createfile - creates a new file";
        advice[i++] = "createdir - creates a new directory";
        advice[i++] = "filedel - deletes specified file or directory";
        advice[i++] = "viewfile [PARAM] - shows detailed information about specified file";
        advice[i++] = "changefile [PARAM] [PARAM] [PARAM] - changes file given as first parameter, by property given as second parameter (which can be uid [uid], gid [gid]"
                      + " or access permissions [perms]),"
                      + " and uses last parameter as the value for specified property";
        /////////////IO\\\\\\\\\\\\\\\\\\\\\
	advice[i++] = "iomount - mounts i/o device";
        advice[i++] = "ioprinttest - printing testing page";
        advice[i++] = "ioshowdefault - printing info about default io device";
        advice[i++] = "iodef - setting default io device";
        advice[i++] = "showdev - printing all devices";
	advice[i++] = "unmount- unmounting all devices";
        advice[i++] = "stmount - mounting removable storage ";
        advice[i++] = "stdef - setting default io device";
        advice[i++] = "stshowdefault - printing info about default storage device";
        advice[i++] = "stuse - displaying usage of storage";
        advice[i++] = "stwritecell - writing character to specified cell in storage memory";
        advice[i++] = "stwritefromcell - writing character from specified cell in storage memory";

        advice[i++] = "stwriteblock - writing string to specified block in storage memory";
        advice[i++] = "stwritestart - writing string to from block=0 to the ond of stream in storage memory";
        advice[i++] = "stsave - saving memory to original file path";
        advice[i++] = "stfiles - show files that are using by storage";
        advice[i++] = "streadcell - reading character from specified cell in storage memory";
        advice[i++] = "streadblock - reading string from specified block in storage memory";
        advice[i++] = "streadfromcell - reading character from specified cell in storage memory";
        advice[i++] = "stread - reading memory from original file path";
        advice[i++] = "streadpath - reading memory from specified file path";
        advice[i++] = "streaddev - reading device infos from specified file path";
        advice[i++] = "stclearmem - clearing storage memory";
        advice[i++] = "stdelpath - deleting device's and data files on memory";
        advice[i++] = "forceswap [PARAM] - runs 10 processes from that file, PARAM is a program name or a path";
        advice[i++] = "printlru - prints content of lru stack";
        advice[i++] = "printfreeframes - prints list of all free frames";
        advice[i++] = "printframe [PARAM] - prints content of a given frame. PARAM is a frame number 0-15";
        advice[i++] = "memstat - shows pages to frames assingnment";
    }

    public static void printAdvice()
    {
        System.out.println("---------:::::::::********* COMMANDS *********:::::::::---------\n");
        System.out.println("[PARAM] means that parameters in the command are expected. [oPARAM] means that the parameter is optional, not necessary.");
        for (String s : Advisor.advice)
        {
            System.out.println(s);
        }
        System.out.println("\n---------:::::::::********* IN COFFEETUX *********:::::::::---------");
    }
}
