package coffeetux.Shell;

import java.util.*;
import java.lang.Iterable;

public class UsersList 
{
    public ArrayList<User> list;
    public User loggedUser; 
    public int lastUserId;
    
    public void changeUserName(String oldName, String newName)
    {
        User u = this.userOfName(oldName);
        u.name = newName;
    }
    
    public void sortById()
    {
        ArrayList<User> tempList = new ArrayList<>();
        User tempUser = new User();
        
        while (this.list.size() != 0)
        {
            int i = Integer.MAX_VALUE;
            for (User u: list)
            {
                if (u.id < i)
                {
                    tempUser = u;
                    i = u.id;
                }
            }
            tempList.add(tempUser);
            this.list.remove(tempUser);
        }
        this.list = tempList;
    }
    
    private void printAllUsersBase(String text)
    {
        {
            System.out.println("[.] Printing all users' " + text + ": \n");
            for (User u: this.list)
            {
                if(text.equals("names"))
                {
                    System.out.print("\tName: " + u.name + "\n");
                }
                
                if(text.equals("id's"))
                {
                    System.out.print("\tId: " + u.id + "\n");
                }
                
                if(text.equals("gid's"))
                {
                    System.out.print("\tGid: " + u.gid + "\n");
                }
            }
            System.out.println();
        }
    }
    
    public void printAllUsers()
    {
        System.out.println("[.] Printing all users:");
        for (User u: this.list)
        {
            System.out.println("\tName: " + u.name);
            System.out.println("\tID: " + u.id);
            System.out.println("\tGID: " + u.gid);
            System.out.println();
        }
    }
    
    public void printAllUsersNames()
    {
        this.printAllUsersBase("names");
    }
    
    public void printAllUsersIDs()
    {
        this.printAllUsersBase("id's");
    }
    
    public void printAllUsersGIDs()
    {
        this.printAllUsersBase("gid's");
    }
    
    /*
    public int findAdequateId()
    {
        this.sortById();
        int i = 1;
        while (this.containsId(i) != false)
        {
            i++;
        }
        return i;
        
    }*/
        
    
    public void refactorUsers()
    {
        int i = 0;
        for (User u: this.list)
        {
            u.id = i;
            i++;
            this.lastUserId = i;
        }
    }
    
    public User userOfName(String name)
    {
        for (User u: this.list)
        {
            if(u.name.equals(name))
            {
                return u;
            }
        }
        return null;
    }
        
    public boolean containsName(String name)
    {
        for (User u: list)
        {
            if (u.name.equals(name))
            {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsId(int id)
    {
        for (User u: list)
        {
            if (u.id == id)
            {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsGid(int gid)
    {
        for (User u: list)
        {
            if (u.gid == gid)
            {
                return true;
            }
        }
        return false;
    }
    
    UsersList()
    {
        this.list = new ArrayList<>();
        this.lastUserId = 0;
    }
}
