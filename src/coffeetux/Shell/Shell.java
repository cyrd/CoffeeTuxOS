package coffeetux.Shell;
// Exceptiony przepisać na ładny niewyjątkowy angielski, zapisywanie list nie tylko na shutdown, tworzenie folderów u Sebastiana po stworzeniu 

import coffeetux.CPU.CPU;
import coffeetux.Process_Management.Process_Management;
import coffeetux.filesystem.FileInfo;
import coffeetux.filesystem.FileSystem;
import coffeetux.filesystem.FileSystemException;
import coffeetux.io.*;
import coffeetux.memory.Memory;
import java.io.Console;
import java.util.Scanner;
import java.io.IOException;
import java.util.ArrayList;

public class Shell
{

    public boolean pause = false;
    public LoginManager usersManager = new LoginManager();
    public boolean exit = false;
    Scanner scan = new Scanner(System.in);

    public FileSystem fileSystem;
    //public String path;
    public FileInfo handle;

    public CPU cpu;
    public Process_Management pm;
    public Memory mem;

    public devicecollector devices = new devicecollector();
    public iodevice io;
    public storagedevice disk;

    public Shell()
    {
        this.usersManager = new LoginManager();
    }

    public void scanCommand()
    {
        String path = "";
        try
        {
            path = FileSystem.ResolvePath(this.handle);
        } catch (FileSystemException ex)
        {
            System.out.println(ex);
        }
        System.out.print("\n[.] Write \"help\" if you need to take a look on the command list.\n" + path + "$");
        Scanner s = new Scanner(System.in);
        String command[] = null;
        command = s.nextLine().split(" ");

        try
        {
            switch (command[0])
            {       //******ACCOUNTS
                case "help":
                {
                    new Advisor();
                    Advisor.printAdvice();
                    break;
                }
                
                case "segregate":
                {
                    this.usersManager.segregate();
                    break;
                }
                
                case "login":
                {
                    this.usersManager.loggedUser = null;
                    if (command.length == 2)
                    {
                        this.usersManager.logInByNameByte(command[1]);
                        FileSystem.Close(handle);
                        try
                        {
                           this.handle = FileSystem.Open("/", 2|4, 2); 
                        }
                        catch (Exception e)
                        {
                        }
                        
                    }
                    if (command.length == 1)
                    {
                        this.usersManager.loginInterface();
                    }
                    if (command.length > 2)
                    {
                        System.out.println("[!] Too many parameters!");
                    }
                    break;
                }

                case "adduser":
                {
                    this.usersManager.addUser();
                    break;
                }

                case "users":
                {
                    if (command.length > 1)
                    {
                        switch (command[1])
                        {
                            case "names":
                            {
                                this.usersManager.usersList.printAllUsersNames();
                                break;
                            }

                            case "id":
                            {
                                this.usersManager.usersList.printAllUsersIDs();
                                break;
                            }

                            case "gid":
                            {
                                this.usersManager.usersList.printAllUsersGIDs();
                                break;
                            }
                        }

                    } else
                    {
                        this.usersManager.usersList.printAllUsers();
                    }
                    break;
                }

                case "groups":
                {
                    if (command.length == 1)
                    {
                        this.usersManager.groupsList.printAllGroupsAndMembers();
                    }
                    if (command.length == 2)
                    {
                        this.usersManager.groupsList.printGroupProperties(this.usersManager.groupsList.groupOfName(command[1]));
                    }
                    if (command.length > 2)
                    {
                        System.out.println("[!] Too many parameters!");
                    }
                    break;
                }

                case "whoami":
                {
                    User u = LoginManager.loggedUser;
                    Group g = this.usersManager.groupsList.groupOfID(u.gid);
                    System.out.println("[.] Your name is " + u.name + ", your id is "
                            + u.id + ", your gid is " + u.gid
                            + ". You belong to the group named " + g.name + ".");
                    break;
                }

                case "addgroup":
                {
                    this.usersManager.addGroupInterface();
                    break;
                }

                case "moveuser":
                {
                    this.usersManager.changeGroup();
                    break;
                }

                case "userdel":
                {
                    this.usersManager.deleteUserInterface();
                    break;
                }

                case "groupdel":
                {
                    this.usersManager.deleteGroupInterface();
                    break;
                }

                case "shutdown":
                {
                    devices.removedevs();
                    devices.iolog.dispose();
                    
                    
                    this.usersManager.saveGroups();
                    this.usersManager.saveUsers();
                    this.exit = true;
                    break;
                }

                case "password":
                {
                    if (command.length == 2)
                    {
                        this.usersManager.newPasswordGetter(this.usersManager.usersList.userOfName(command[1]));
                    }
                    if (command.length == 1)
                    {
                        this.usersManager.changePassword();
                    }
                    if (command.length > 2)
                    {
                        System.out.println("[!] Too many parameters!");
                    }
                    break;
                }
                //FileSystem
                case "createfile":
                {
                    this.createFileInterface();
                    break;
                }

                case "createdir":
                {
                    this.createFolder();
                    break;
                }

                case "cd":  //Moving forward or backwards(..) through the folders
                {
                    if (command.length > 1)
                    {
                        try
                        {
                            this.handle = FileSystem.ChangeDirectory(this.handle, "/" + command[1]);
                        } catch (FileSystemException ex)
                        {
                            System.out.println(ex);
                        }
                    } else
                    {
                        System.out.println("[!] No further destination was stated. Type \"help\" for the list of commands.");
                    }
                    break;
                }

                case "lds":
                {

                    ArrayList<String[]> list;
                    try
                    {
                        list = FileSystem.ListDir(handle);
                        for (int i = 0; i < list.size(); i++)
                        {
                            System.out.println("Name: " + list.get(i)[0] + "\t\t" + list.get(i)[1] + "\t\t" + list.get(i)[2] + "\t\t" + list.get(i)[3]);
                        }
                    } catch (FileSystemException ex)
                    {
                        System.out.println(ex);
                    }

                    break;
                }

                case "viewfile":
                {
                    if (command.length > 1)
                    {
                        try
                        {
                            if (FileSystem.FileExists(path + command[1], 2) != -1)
                            {
                                String[] list = FileSystem.FileInfoToPrint(path + command[1], 2);
                                for (int i = 0; i < list.length; i++)
                                {
                                    System.out.println(list[i]);
                                }
                            }
                        } catch (FileSystemException ex)
                        {
                            System.out.println(ex);
                        }
                    }
                    break;
                }

                case "changefile":
                {
                    if (command.length == 4)
                    {
                        try
                        {
                            FileInfo tmp = FileSystem.Open(path + command[1], FileSystem.F_READ, 2);
                            switch (command[2])
                            {
                                case "uid":
                                    FileSystem.SetFileUID(tmp, Integer.parseInt(command[3]));
                                    break;
                                case "gid":
                                    FileSystem.SetFileGID(tmp, Integer.parseInt(command[3]));
                                    break;
                                case "perms":
                                    FileSystem.SetFilePermissions(tmp, Integer.parseInt(command[3]));
                                    break;
                            }
                            FileSystem.Close(tmp);
                        } catch (FileSystemException ex)
                        {
                            System.out.println(ex);
                        }
                    }
                    break;
                }

                case "exec":        //***WORKING WITH MEMORY
                {                   //Executing file
                    if (command.length > 1)
                    {
                        try
                        {
                            FileInfo tmp = FileSystem.Open(path + command[1], FileSystem.F_READ | FileSystem.F_WRITE, 2);
                            if (FileSystem.GetFileType(tmp) == 3 || FileSystem.GetFileType(tmp) == 4)
                            {
                                pm.exec(pm.fork(pm.process_list.get(0)), path + command[1], 2 | 4, 1, this.usersManager.getId(), this.usersManager.getGid());
                            } else
                            {
                                System.out.println("Wrong file type.");
                            }
                            FileSystem.Close(tmp);

                        } catch (FileSystemException ex)
                        {
                            System.out.println(ex);
                        }
                    } else
                    {
                        System.out.println("Error 601: No arguments!");
                    }
                    break;
                }

                case "editor":
                {
                    String text = "";
                    String line = "";
                    while (!line.equals("end"))
                    {
                        line = this.scan.nextLine();
                        if (!line.equals("end"))
                        {
                            text = text + line + "\n";
                        }
                    }
                    System.out.println("[?] Type the name of the file to write/overwrite.");
                    line = this.scan.nextLine();
                    try
                    {
                        String name[] = line.split("\\.");
                        FileSystem.CreateFile(FileSystem.ResolvePath(this.handle) + name[0], 2, 740);
                        FileInfo tmp = FileSystem.Open(FileSystem.ResolvePath(this.handle) + name[0], FileSystem.F_READ | FileSystem.F_WRITE, 2);
                        FileSystem.Truncate(tmp);
                        FileSystem.Write(tmp, text);
                        if (line.contains(".exe"))
                        {
                            FileSystem.SetFileType(tmp, 3);
                        }
                        FileSystem.Close(tmp);
                    } catch (Exception ex)
                    {
                        System.out.println(ex);
                    }
                    break;
                }
                
                /*case "clarify":
                {
                    this.usersManager.deleteEmptyGroups();
                    break;
                }*/

                case "chuname":
                {
                    this.usersManager.changeUserName();
                    break;
                }

                case "chgname":
                {
                    this.usersManager.changeGroupName();
                    break;
                }

                case "tornado":
                {
                    this.tornado();
                    break;
                }

                case "kill": //!!!!!!!!!
                {
                    if (command.length > 1)
                    {
                        this.pm.kill(Integer.parseInt(command[1]));

                    } else
                    {
                        System.out.println("Error! Wrong argument!");
                    }
                    break;

                }

                case "ps":
                {
                    this.pm.printprocesses();
                    break;
                }

                case "filedel":
                {
                    if (command.length > 1)
                    {
                        try
                        {
                            FileSystem.Delete(FileSystem.ResolvePath(handle) + command[1], 2);
                        } catch (FileSystemException ex)
                        {
                            System.out.println(ex);
                        }
                    }
                    break;
                }

                ///////////DEVICES HANDLING
                case "iomount":
                {
                    try
                    {
                        FileSystem.CreateDir("/mnt/", 1, 777);
                        String name;
                        Scanner z = new Scanner(System.in);
                        System.out.println("IO name:");
                        name = z.nextLine();
                        iodevice temp = new iodevice(name);
                        if (devices.iodevs.isEmpty() == true)
                        {
                            temp.def = true;
                            devices.iodevs.addFirst(temp);
                            io = devices.iodevs.getFirst();

                        } else
                        {
                            devices.addio(temp);
                        }
                        int d = devices.iodevs.size() - 1;
                    } catch (Exception e)
                    {
                        devices.iolog.println(e);
                    }
                    break;
                }



                case "ioprinttest":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else
                    {
                        io.cout(io.name + " - Testing page");
                    }
                    break;
                }
                case "iodef":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else
                    {
                        devices.showio();
                        io.cout("Select default device: ");
                        Scanner z = new Scanner(System.in);
                        int x = Integer.parseInt(z.nextLine());
                        if (devices.iodevs.get(x) != null)
                        {
                            for (int i = 0; i < devices.iodevs.size(); i++)
                            {
                                if (devices.iodevs.get(i).def == true)
                                {
                                    devices.iodevs.get(i).def = false;
                                }
                            }
                            devices.iodevs.get(x).def = true;
                            io = devices.iodevs.get(x);
                            System.out.println(io.name + "is now default");
                        } else
                        {
                            System.out.println("Incorrect id of device");
                        }

                    }
                    break;
                }
                case "ioshowdefault":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else
                    {
                        io.show();
                    }
                    break;
                }
                case "showdev":
                {
                    try
                    {
                        devices.showdevices();
                    } catch (Exception e)
                    {
                        devices.iolog.println(e);
                    }
                    break;
                }
                case "unmount":
                {
                    try
                    {
                        iodevice tempio;
                         tempio=devices.iodevs.getFirst();
                        devices.removedevs();
                            devices.iodevs.addFirst(tempio);
                            devices.iodevs.getFirst().def=true;
                        for (int i = 0; i < devices.storagedevs.size(); i++)
                        {
                            devices.storagedevs.get(i).deletefiles(io);
                            FileSystem.Delete("/mnt/" + devices.storagedevs.get(i).name, 1);
                            devices.iolog.println("Removing: " + devices.storagedevs.get(i).name + ", with local path /mnt/" + devices.storagedevs.get(i).name);
                        }
                        devices.storagedevs.clear();
                        io.cout("All devices removed!");
                        System.out.println();
                    } catch (Exception e)
                    {
                        devices.iolog.println("Error unmounting!");
                        System.out.println(e);
                    }
                    break;
                }

                case "stmount":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else
                    {

                        try
                        {
                            String name;
                            Scanner z = new Scanner(System.in);
                            io.cout("Storage name:");
                            name = z.nextLine();

                            storagedevice temp = new storagedevice(name);

                            if (devices.storagedevs.isEmpty() == true)
                            {
                                temp.def = true;
                                devices.storagedevs.addFirst(temp);
                                disk = devices.storagedevs.getFirst();
                            } else
                            {
                                devices.addstorage(temp);
                            }

                            FileSystem.CreateFile("/mnt/" + name, 1, 777);
                            FileInfo fi = FileSystem.Open("/mnt/" + name, FileSystem.F_WRITE | FileSystem.F_READ, 1);
                            FileSystem.SetFileType(fi, 4);
                            FileSystem.Close(fi);
                        } catch (Exception e)
                        {
                            devices.iolog.println(e);
                        }

                    }
                    break;
                }

                case "stdef":
                {
                    if (disk == null)
                    {
                        io.cout("First mount storage device");
                    } else
                    {
                        devices.showst();
                        io.cout("Select default device: ");
                        Scanner z = new Scanner(System.in);
                        int x = Integer.parseInt(z.nextLine());
                        if (devices.storagedevs.get(x) != null)
                        {
                            for (int i = 0; i < devices.storagedevs.size(); i++)
                            {
                                if (devices.storagedevs.get(i).def == true)
                                {
                                    devices.storagedevs.get(i).def = false;
                                }
                            }
                            devices.storagedevs.get(x).def = true;
                            disk = devices.storagedevs.get(x);
                            System.out.println(disk.name + "is now default");
                        } else
                        {
                            io.cout("Incorrect id of device");
                        }

                    }
                    break;
                }
                case "stshowdefault":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    }
                    if (disk == null)
                    {
                        io.cout("First mount storage device");
                    } else
                    {
                        disk.show(io);
                    }
                    break;
                }
                case "stuse":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                        System.out.println();
                    } else
                    {
                        disk.usage(io);
                    }
                    break;
                }
                case "stwritecell":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                        System.out.println();
                    } else
                    {
                        int x, y;
                        char ch;
                        Scanner z = new Scanner(System.in);
                        System.out.println("Type x [x,y]: ");
                        x = Integer.parseInt(z.nextLine());
                        System.out.println("Type y [x,y]: ");
                        y = Integer.parseInt(z.nextLine());
                        System.out.println("Type character: ");
                        ch = io.cin(z.nextLine()).charAt(0);
                        disk.writememorycell(x, y, ch, io);
                    }
                    break;
                }
                case "stwritefromcell":
                {
                    int i = 0;
                    String data = "";
                    System.out.println("Give number of cell, 0-511: ");
                    Scanner z = new Scanner(System.in);
                    i = Integer.parseInt(z.nextLine());
                    System.out.println("Type data: ");
                    data = io.cin(z.nextLine());
                    System.out.println();
                    disk.writememoryfromcell(i, data, io);
                    break;
                }
                case "streadfromcell":
                {
                    int i = 0;
                    int j = 0;
                    System.out.println("Give number of cell, 0-511: ");
                    Scanner z = new Scanner(System.in);
                    i = Integer.parseInt(z.nextLine());
                    System.out.println("Give length: ");
                    j = Integer.parseInt(z.nextLine());

                    disk.readmemoryfromcell2(i, j, io);
                    break;
                }
                case "stwriteblock":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                        System.out.println();
                    } else
                    {
                        Scanner z = new Scanner(System.in);
                        System.out.println("Type block number");
                        int x = Integer.parseInt(z.nextLine());
                        disk.writeblock(x, io);
                    }
                    break;
                }
                case "stwritestart":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                        System.out.println();
                    } else
                    {
                        disk.writememoryfromstart(io);
                    }
                    break;
                }
                case "stsave":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                    } else
                    {
                        disk.savememory(io);
                    }
                    break;
                }
                case "stfiles":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                    } else
                    {
                        disk.showusingfiles(io);
                    }
                    break;
                }
                case "streadcell":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                    } else
                    {
                        Scanner z = new Scanner(System.in);
                        System.out.println("Type x [x,y]: ");
                        int x = Integer.parseInt(z.nextLine());
                        System.out.println("Type y [x,y]: ");
                        int y = Integer.parseInt(z.nextLine());
                        disk.readmemorycell(x, y, io);
                    }
                    break;
                }
                case "streadblock":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                        System.out.println();
                    } else
                    {
                        Scanner z = new Scanner(System.in);
                        io.cout("Type block number:");
                        System.out.println();
                        int x = Integer.parseInt(z.nextLine());
                        disk.readblock(x, io);
                    }
                    break;
                }
                case "stread":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                        System.out.println();
                    } else
                    {
                        disk.readmemory(io);
                    }
                    break;
                }
                case "streadpath":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                        System.out.println();
                    } else
                    {
                        Scanner z = new Scanner(System.in);
                        io.cout("Type path:");
                        System.out.println();
                        String x = io.cin(z.nextLine());
                        
                        disk.readmemory(x, io);
                    }
                    break;
                }
                case "streaddev":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                        System.out.println();
                    } else
                    {
                        Scanner z = new Scanner(System.in);
                        io.cout("Type path of device file:");
                        String x = io.cin(z.nextLine());
                        disk.readdevice(x, io);
                    }
                    break;
                }

                case "stclearmem":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                        System.out.println();
                    } else
                    {
                        disk.clearmemory(io);
                    }
                    break;
                }
                case "stdelpath":
                {
                    if (io == null)
                    {
                        System.out.println("First mount io device");
                    } else if (disk == null)
                    {
                        io.cout("First mount storage device");
                    System.out.println();
                    } else
                    {
                        disk.deletefiles(io);
                    }
                    break;

                }

                

                //MEMORY COMMANDS!
                case "forceswap":
                {
                    if (command.length > 1)
                    {
                        try
                        {
                            FileInfo tmp = FileSystem.Open(path + command[1], FileSystem.F_READ | FileSystem.F_WRITE, 2);
                            if (FileSystem.GetFileType(tmp) == 3 || FileSystem.GetFileType(tmp) == 4)
                            {
                                for (int i = 0; i < 10; i++)
                                {
                                    pm.exec(pm.fork(pm.process_list.get(0)), path + command[1], 2 | 4, 1, this.usersManager.getId(), this.usersManager.getGid());
                                }
                            } else
                            {
                                System.out.println("Wrong file type.");
                            }
                            FileSystem.Close(tmp);
                        } catch (FileSystemException ex)
                        {
                            System.out.println(ex);
                        }
                    } else
                    {
                        System.out.println("Error 601: No arguments!");
                    }
                    break;

                }
                case "printlru":
                {

                    mem.PrintShellLRU();

                    break;
                }
                case "printfreeframes":
                {
                    mem.PrintFreeFrames();

                    break;
                }
                case "printframe":
                {
                    if (command.length > 1)
                    {
                        int x = Integer.parseInt(command[1]);
                        mem.PrintFrame(x);
                    } else
                    {
                        System.out.println("Error 601: No arguments!");
                    }

                    break;
                }

                case "memstat":
                {
                    mem.MemoryStatus();
                    break;
                }

                case "pause":
                {
                    pause = !pause;
                    break;
                }

                default:
                {
                    System.out.println("[!] Such command does not exist. Type \"help\" for the list of commands.");
                }
            }

        } catch (NumberFormatException x)
        {
            System.out.println("Incorrect command!");
        }
        /*   
         try
         {
         System.out.println("!&&!Press enter...!&&!");
         System.in.read();
         } catch (IOException x)
         {
         };
                
         */
    }

    public void startFileSystem()
    {
        try
        {
            this.handle = FileSystem.Open("/", FileSystem.F_READ, 2);
        } catch (Exception e)
        {
            System.out.println(e);
        }
        /*  for (User u : this.usersManager.usersList.list)
         {
         FileSystem.CreateDir("/" + u.name + "/", 2, 740);
         }
         }
         catch (FileSystemException ex)
         {
         System.out.println(ex);
         }*/
    }

    public void createFolder()
    {
        System.out.println("\n[?] What's the name of the folder?");
        String text = this.scan.nextLine();
        try
        {
            FileSystem.CreateDir(FileSystem.ResolvePath(this.handle) + text, 2, 740);
        } catch (FileSystemException ex)
        {
            System.out.println(ex);
        }
    }

    public static void createUserFolder(User u)
    {
        try
        {
            FileSystem.CreateDir("/" + u.name, 2, u.id, u.gid, 740);
        } catch (FileSystemException ex)
        {
            System.out.println(ex);
        }
    }

    public void tornado()
    {
        this.usersManager.groupsList.list.clear();
        this.usersManager.usersList.list.clear();
        this.usersManager.usersList.lastUserId = 0;
        this.usersManager.groupsList.maxNumber = 0;
        this.usersManager.saveGroups();
        this.usersManager.saveUsers();
        this.usersManager.startUser();
    }

    /* public void deleteUserFolder(User deleted)
     {
     }*/
    public void createFileInterface()
    {
        try
        {
            System.out.println("[?] What name do you want for the file to have?");
            String filename = this.scan.nextLine();
            FileSystem.CreateFile(FileSystem.ResolvePath(this.handle) + filename, 2, 740);
            System.out.println("[.] The file was succesfully created.");
        } catch (FileSystemException ex)
        {
            System.out.println(ex);
        }
    }
}
