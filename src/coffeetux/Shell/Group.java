package coffeetux.Shell;

import java.util.*;

public class Group 
{
    ArrayList<User> groupUsers;
    String name;
    int gid;
    
    Group()
    {
        this.gid = -1;
        this.name = "INVALID";
        this.groupUsers = new ArrayList<>();
    }
    
    Group(String name, int gid)
    {
        this.name = name;
        this.gid = gid;
        this.groupUsers = new ArrayList<>();
    }
    
    public void printGroupMembers()
    {
        for (User u: this.groupUsers)
        {
            System.out.println("Name: " + u.name);
            System.out.println("ID: " + u.id);
            System.out.println("GID: " + u.gid);
            System.out.println();
        }
    }
}
