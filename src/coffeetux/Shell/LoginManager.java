package coffeetux.Shell;

import coffeetux.filesystem.FileSystem;
import coffeetux.filesystem.FileSystemException;
import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.util.Arrays.*;
import java.util.Scanner;


public class LoginManager 
{
    GroupList groupsList;
    UsersList usersList;
    Scanner scan;
    static User loggedUser;
    public File file;
    public File fileGroups;
 
    public static int getId()
    {
        return LoginManager.loggedUser.id;
    }

    public static int getGid()
    {
        return LoginManager.loggedUser.gid;
    }

    public static String getName()
    {
        return LoginManager.loggedUser.name;
    }
    
    //Tylko admini mogą przenosić użytkowników
    //Tylko admini mogą dodawać użytkowników do domyślnych ich grup
    //Tylko root może usuwać użytkowników innych niż zalogowany
    //Tylko root może usuwać grupy użytkowników

    
    public void loginInterface()
    {
        while(LoginManager.loggedUser == null || LoginManager.loggedUser.name.equals("INVALID") )
        {
            this.usersList.printAllUsersNames();
            System.out.println(" \n[?] What's the name of the user you want to log as?");
            Scanner s = new Scanner(System.in);
            String name = s.nextLine();
            switch (this.logInByNameByte(name))
            {
                case 0:
                {
                    System.out.println("[!] No user with name " + name + " was found");
                    break;
                }
                
                case 1:
                {
                    System.out.println("[!] Password mismatch!");
                    break;
                }
                
                case 2:
                {
                    System.out.println("[.] Password correct. User " + name + " is logged.");
                    break;
                }
                
                case 3:
                {
                    System.out.println("[.] User " + name + " is logged.");
                    break;
                }
            }
        }
    }
   
    public byte logInByNameByte(String name)
    {
        if (this.usersList.containsName(name))
        {
            for(User user: this.usersList.list)
            {
                if(user.name.equals(name))
                {
                    if (user.hash == null || user.hash.equals("null"))
                    {
                        this.loggedUser = user;
                        return 3;
                    }
                    if (this.checkPassword(user.hash) == true)
                    {
                        LoginManager.loggedUser = user;
                        return 2;
                    }
                    return 1;
                }
            }
        }
        return 0;
    }
    
    public void addUser(User u)
    {
        this.usersList.list.add(u);
    }
    
    public void segregate()
    {
        int i = 0;
        for (User u: this.usersList.list)
        {
            u.id = i;
            this.usersList.lastUserId = i + 1;
            i++;
        }
        i = 0;
        for(Group g: this.groupsList.list)
        {
            g.gid = i;
            for(User u: g.groupUsers)
            {
                u.gid = i;
                this.groupsList.maxNumber = i+1;
            }
            i++;
        }
        this.saveAll();
    }
    
    public void addGroup(Group g)
    {
        this.groupsList.list.add(g);
    }
    
    public void changeUserName()
    {
        if (LoginManager.getId() == 0)
        {
            System.out.println("[?] What user's name do you want to change?");
            String name = this.scan.nextLine();
            if (!name.equals("root") && this.usersList.containsName(name))
            {
                System.out.println("[?] Type the new name.");
                String newName = this.scan.nextLine();
                this.usersList.changeUserName(name, newName);
                System.out.println("[.] User, who had had name " + name + " is now"
                        + " named " + newName + ".");
            }
            else
            {
                System.out.println("[!] Users does not exist or trying to change root's name.");
            }
        }
        else
        {
            System.out.println("[?] Type the new name for yourself.");
            String newName = this.scan.nextLine();
            this.usersList.changeUserName(LoginManager.getName(), newName);
            System.out.println("[.] Your name is " + newName + " since now.");
        }
    }
    
    public void changeGroupName()
    {
        if (LoginManager.getId() == 0)
        {
            System.out.println("[?] What group's name do you want to change?");
            String name = this.scan.nextLine();
            if (!name.equals("Admins") && !name.equals("Users") && this.groupsList.containsName(name))
            {
                System.out.println("[?] Type the new name.");
                String newName = this.scan.nextLine();
                this.groupsList.changeGroupName(name, newName);
                System.out.println("[.] User, who had had name " + name + " is now"
                        + " named " + newName + ".");
            }
            else
            {
                System.out.println("[!] Group does not exist or trying to change Admin's group name.");
            }
        }
        else
        {
            System.out.println("[!] Only root can change group's name!");
        }
    }
    
    public void saveAll()
    {
        this.saveUsers();
        this.saveGroups();
    }
    
    public void saveUsers()
    {
        PrintWriter printer = null;
        try
        {
            printer = new PrintWriter("users_file.txt");
        }
        catch (FileNotFoundException e)
        {
            System.out.println("[!] Error occured when saving!");
        }

        for (User u : this.usersList.list)
        {
            printer.println(u.name);
            printer.println(u.id);
            printer.println(u.gid);
            printer.println(u.hash);
        }
        printer.close();
    }

    public void saveGroups()
    {
        PrintWriter printer = null;
        try
        {
            printer = new PrintWriter("groups_file.txt");
        }
        catch (FileNotFoundException e)
        {
            System.out.println("[!] Error occured when saving groups of users!");
        }

        for (Group g : this.groupsList.list)
        {
            printer.println(g.name);
            printer.println(g.gid);
        }
        printer.close();
    }

    public void getUsers()
    {
        Scanner fileScanner = null;
        try
        {
            fileScanner = new Scanner(this.file);

            while (fileScanner.hasNext())
            {
                String name = fileScanner.nextLine();
                int id = Integer.parseInt(fileScanner.nextLine());
                int gid = Integer.parseInt(fileScanner.nextLine());
                String hash = fileScanner.nextLine();
                this.usersList.lastUserId = id + 1;
                
                User tempUser = new User(name, id, gid, hash);

                Group tempGroup = this.groupsList.groupOfID(tempUser.gid);
                if (tempGroup != null)
                {
                    tempGroup.groupUsers.add(tempUser);
                    System.out.println("[R] User named " + tempUser.name + " of id == " + tempUser.id
                                       + " was added to the group named " + tempGroup.name + " of gid == " + tempGroup.gid + ".");
                }
                else
                {
                    System.out.println("[!] Error occured ");
                }
                this.usersList.list.add(tempUser);
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println(e);
        }
    }

    public void getGroups()
    {
        Scanner fileScanner = null;
        try
        {
            fileScanner = new Scanner(this.fileGroups);
            while (fileScanner.hasNext())
            {
                Group tempGroup = new Group();
                tempGroup.name = fileScanner.nextLine();
                tempGroup.gid = Integer.parseInt(fileScanner.nextLine());
                this.addGroup(tempGroup);
                this.groupsList.maxNumber = tempGroup.gid + 1;
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println(e);
        }
    }

    public boolean noUsers()
    {
        return (this.usersList.list.isEmpty());
    }
    
    public String hashByMD5(String text) throws java.security.NoSuchAlgorithmException
    {
        MessageDigest encrypter = MessageDigest.getInstance("MD5");
        StringBuffer textDigest = new StringBuffer();
        byte[] coded = encrypter.digest(text.getBytes());
        for (byte b : coded)
        {
            textDigest.append(String.format("%02x", b & 0xff));
        }
        return textDigest.toString();
    }
    
    public void changePassword()        //tylko root moze zmienić hasło komuś
    {
        if (LoginManager.getName().equals("root"))
        {
            Scanner s = new Scanner(System.in);
            System.out.println("[.] Type the name of the user you want to change password.");
            String name = s.nextLine();
            User changed = this.usersList.userOfName(name);
            if (changed != null)
            {
                this.newPasswordGetter(changed);
            }
            else
            {
                System.out.println("[!] User of that name does not exist!");
            }
        }
        else
        {
            this.newPasswordGetter(this.loggedUser);
        }
    }
    
    
    public boolean createUser(String name, int gid)
    {
        if (this.usersList.containsName(name) != true)
        {
            if (this.groupsList.hasGroupOfID(gid))
            {
                User temp = new User(name, this.usersList.lastUserId, gid);
                this.usersList.list.add(temp);
                this.usersList.lastUserId++;
                Group toAdd = this.groupsList.groupOfID(gid);
                toAdd.groupUsers.add(temp);
                System.out.println("[!] Main list of users and the list with name " + toAdd.name + ", gid == " + temp.gid + " extended by an user with id: "
                   + temp.id + " and name " + temp.name + ".");
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }
    
    public void startUser()
    {
        this.getUsers();
        if (this.noUsers() == true)
        {
            this.createRootAndFirstGroups();
            this.saveAll();
            System.out.println("[!] Creating root as no account was created in system.");
            System.out.println("[.] Logged as " + this.loggedUser.name);
            Shell.createUserFolder(this.usersList.userOfName("root"));
        }
        else
        {
            this.loginInterface();
        }
    }
    
    public void createRootAndFirstGroups()
    {
        System.out.println("[!] System started as a clean one. Starting ordinary "
                           + "routine of creating basic users groups and root account.\n");

        if (this.groupsList.list.isEmpty())
        {
            this.groupsList.createBasicGroups();
            this.createUser("root", 0);
            User root = this.usersList.userOfName("root");
            LoginManager.loggedUser = root;
            //System.out.println("[!] Main list of users extended by an user with id: "
            //  + root.id + " and name " + root.name + ". It is a privileged user.");

            Group temp = this.groupsList.groupOfID(0);
            //temp.groupUsers.add(root);
            System.out.println("[!] List of members of group " + temp.name
                               + " extended by an user with id: " + root.id + " and name " + root.name + ".");
        }
    }
    
    public void createDefaultUser(String name)
    {
        if (!this.usersList.containsName(name) && !name.equals("") && !this.groupsList.containsName(name))
        {
            User temp = new User(name, this.usersList.lastUserId, this.groupsList.maxNumber);
            Group tempGroup = new Group(name, this.groupsList.maxNumber);
            this.groupsList.addGroup(tempGroup);
            this.usersList.list.add(temp);
            //this.groupsList.addUserToDefaultGroup(temp);
            this.groupsList.addUserToGroupOfID(temp, this.groupsList.maxNumber);
            this.groupsList.maxNumber++;
            this.usersList.lastUserId++;
            this.saveAll();
            System.out.println("[.] User " + name + " was successfully created.");
            System.out.println("[!] Main list of users and the list with name " + tempGroup.name + ", gid == " + temp.gid + " extended by an user with id: "
                               + temp.id + " and name " + temp.name + ".");
            Shell.createUserFolder(temp);
        }
        else
        {
            System.out.println("[!] Username not available. You may want to see the list of used nicknames (type \"users names\") and try again.");
        }
    }

    public void addUser()
    {
        System.out.print("[?] What name do you want to register? \n$");
        String name = this.scan.nextLine();

        if (LoginManager.getGid() == 0)
        {
            this.createDefaultUser(name);
        }
        else
        {
            if (LoginManager.getGid() != 1)
            {
                this.createUser(name, LoginManager.getGid());  
            }
            else
            {
                System.out.println("[!] You have no permission to add users to this group.");
            }
        }
    }
    
    public void deleteUser(String name)
    {
        if (this.usersList.containsName(name))
        {
            for (User u : this.usersList.list)
            {
                if (u.name.equals(name))
                {
                    if (LoginManager.getId() == 0 && !name.equals("root"))
                    {
                        Group noLonger = this.groupsList.groupOfID(u.gid);
                        this.groupsList.deleteUserFromGroupPerm(u);
                        this.usersList.list.remove(u);
                        if (u.name.equals(noLonger.name) && noLonger.groupUsers.isEmpty())
                        {
                            this.deleteGroupNoSerialize(noLonger);
                        }
                        this.saveAll();
                        System.out.println("[.] User " + name + " was deleted.");
                        try
                        {
                            FileSystem.Delete("/" + name, 2);
                        }
                        catch (FileSystemException ex)
                        { 
                            System.out.println(ex);
                        }
                        break;
                    }

                    if (LoginManager.getGid() == 0 && name.equals("root"))
                    {
                        System.out.println("[!] Could not delete root account!");
                        break;
                    }

                    if (LoginManager.getGid() != 0 && name.equals(LoginManager.getName()))
                    {
                        this.groupsList.deleteUserFromGroupPerm(u);
                        this.usersList.list.remove(u);
                        this.saveAll();
                        System.out.println("[.] User " + name + " was deleted.");
                        System.out.println("[!] Before deleting the account, you "
                                           + "were logged out! You have to log to another account.");
                        LoginManager.loggedUser = null;
                        this.loginInterface();

                        break;
                    }
                    else
                    {
                        System.out.println("[!] No permission to delete this account!");
                        break;
                    }
                }
            }
        }
        else
        {
            System.out.println("[!] User " + name + " does not exist!");
        }
    }
    
    public void serialize(Group g)      //writes all users from the group to the default group
    {
        Group serialGroup = this.groupsList.groupOfID(1);
        for (User u : g.groupUsers)
        {
            //g.groupUsers.remove(u);
            u.gid = 1;
            serialGroup.groupUsers.add(u);
        }

    }

    public void deleteGroupInterface()
    {
        System.out.println("[?] What's the name of the group you want to delete?");
        String name = this.scan.nextLine();
        Group delete = this.groupsList.groupOfName(name);

        if(delete != null)
        {
            if (LoginManager.loggedUser.id == 0 && delete.gid != 0 && delete.gid != 1)
            {
                this.deleteGroup(delete);
                System.out.println("[.] Group of name " + delete.name + " was deleted. "
                                    + "All the users were moved to the default group with matching gid.");    
            }
            else
            {
                System.out.println("[!] You have no permission to delete this group!");
            }
        }
        else
        {
            System.out.println("[!] Group does not exist!");
        }
    }

    public void deleteUserInterface()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("[?] What's the name of the user you want to delete?");
        String name = in.nextLine();

        User delete = this.usersList.userOfName(name);
        if (delete != null)
        {
            this.deleteUser(name);
            //System.out.println("[.] User " + delete.name + " was deleted.");
        }
        else
        {
            System.out.println("[!] User named " + name + " does not exist.");
        }
    }

    public void deleteGroup(Group g)
    {
        this.serialize(g);
        Group delete = this.groupsList.groupOfID(g.gid);
        this.groupsList.list.remove(delete);
        this.saveGroups();
    }
    
    public void deleteGroupNoSerialize(Group g)
    {
        Group delete = this.groupsList.groupOfID(g.gid);
        this.groupsList.list.remove(delete);
        this.saveGroups();
    }

    public void addGroupInterface()
    {
        if (LoginManager.loggedUser.gid == 0)
        {
            Scanner in = new Scanner(System.in);
            System.out.print("[?] What name do you want for the group to have? \n$");
            String name = in.nextLine();
            this.groupsList.createGroup(name);
            //Group temp = this.groupsList.groupOfName(name);
            this.saveGroups();
            //System.out.println("[.] Group named " + temp.name + " with gid " + temp.gid + " was succesfully created.");
        }
        else
        {
            System.out.println("[!] You are not permitted to add groups! "
                               + "Only Admins can add groups. Try \"login\" command and log as root.");
        }
    }

    public void changeGroup()
    {
        if (this.loggedUser.gid == 0)
        {
            Scanner in = new Scanner(System.in);
            System.out.print("[?] What username has the user you want to move?\n$");
            String name = in.nextLine();
            if (this.usersList.containsName(name))
            {
                User u = this.usersList.userOfName(name);
                System.out.println("[?] What's the target group name?");
                String nameGroup = in.nextLine();
                Group target = this.groupsList.groupOfName(nameGroup);
                Group noLonger = this.groupsList.groupOfID(u.gid);
                if (target != null)
                {
                    noLonger.groupUsers.remove(u);
                    target.groupUsers.add(u);
                    if (u.name.equals(noLonger.name) && noLonger.groupUsers.isEmpty())
                    {
                        this.deleteGroup(noLonger);
                    }
                    u.gid = target.gid;
                    this.saveAll();
                    System.out.println("[.] The user " + u.name
                                       + " was removed from the list of group named "
                                       + noLonger.name + " and added to the list named " + target.name);
                }
                else
                {
                    System.out.println("[!] There's no such group.");
                }
            }
            else
            {
                System.out.println("[!] There's no user having that name.");
            }
        }
        else
        {
            System.out.println("[!] You are not permitted move users! Only admins can move users. Try \"login\" command and log as root.");
        }
    }
    
    public void newPasswordGetter(User u)
    {
        Scanner s = new Scanner(System.in);
        System.out.println("[.] Type the new password twice.");
        String[] pass = new String[2];
        try
        {
            pass[0] = this.hashByMD5(s.nextLine());
            pass[1] = this.hashByMD5(s.nextLine());
        }
        catch(Exception e)
        {
            pass[0] = "a";
            pass[1] = "b";
        }

        if (pass[0].equals(pass[1]))
        {
            System.out.println("[.] Password changed.");
            u.hash = pass[0];
            if (pass[0].equals("d41d8cd98f00b204e9800998ecf8427e"))     //Hash generowany przez pusty String
            {
                u.hash = "null";
            }
            this.saveUsers();
        }
        else
        {
            System.out.println("[.] Passwords don't match.");
        }
    }
    
    
    /*public void array()
    {
        char[] pass = new char[128];
        int i = 0;
        do
        {
            try 
            {
                pass[i] = (char) System.in.read();
                System.out.print("**");
            } catch (IOException ex) 
            {
                System.out.println("Error by char array!");
            }
            i++;
        } while (pass[i-1] != '\n');
        String out = new String(pass);
        System.out.println(out.split("") + "   " + out.length());
    }*/
    
    public boolean checkPassword(String hash)
    {
        Scanner in = new Scanner(System.in);
        String passwordHash = "";
        System.out.println("[!] Type the password.");
        try
        {
            passwordHash = this.hashByMD5(in.nextLine());
        }
        catch (Exception e)
        {
        }
        if (passwordHash.equals(hash))
        {
            System.out.println("[C] " + passwordHash + " is equal to " + hash);
            return true;
        }
        System.out.println("[C] " + passwordHash + " is not equal to " + hash);
        return false;
    }
    
    /*public void deleteEmptyGroups()
    {
        if(LoginManager.getGid() == 0)
        {
            for(Group g: this.groupsList.list)
            {
                if(g.groupUsers.isEmpty() && !(g.gid < 2))
                {
                    this.deleteGroup(g);
                    System.out.println("[.] Group named " + g.name + " was deleted.");
                }                    
            }
        }
        else
        {
            System.out.println("[!] You have no permission to this action.");
        }
    }*/
    
    LoginManager()
    {
        LoginManager.loggedUser = new User();
        this.usersList = new UsersList();
        this.groupsList = new GroupList();
        this.file = new File("users_file.txt"); 
        /*
        try
        {
           
        }
        catch (Exception e)
        {
            System.out.println("[!] Creating user's file as no users were ever created.");
        }*/
        this.fileGroups = new File("groups_file.txt");
        this.scan = new Scanner(System.in);
    }
}
