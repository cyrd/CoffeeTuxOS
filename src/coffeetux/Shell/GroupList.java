package coffeetux.Shell;

import java.util.*;

public class GroupList 
{
    ArrayList<Group> list;
    public int maxNumber;
    
    public void changeGroupName(String oldName, String newName)
    {
        Group g = this.groupOfName(oldName);
        g.name = newName;
    }
    
    public void addGroup(Group group)
    {
        this.list.add(group);
    }
    
    public void addUserToHisGroup(User user)      //dodaje user do grupy, którą ma wskazaną w polu
    {
        Group temp = this.groupOfID(user.gid);
        if(temp != null)
        {
            temp.groupUsers.add(user);
        }
    }
    
    public void addUserToGroupOfID(User user, int gid)  //Dodaje usera do grupy wskazanej przez parametr
    {
        Group temp = this.groupOfID(gid);
        if (temp != null)
        {
            temp.groupUsers.add(user);
        }
    }
    
    public void addUserToDefaultGroup(User user)
    {
        Group temp = this.groupOfID(1);
        user.gid = 1;
        temp.groupUsers.add(user);
    }
    
    public void deleteUserFromGroup(User user)       // 
    {
        Group group = this.groupOfID(user.gid);
        if(group != null)
        {
            group.groupUsers.remove(user);
            this.addUserToDefaultGroup(user);
        }
    }
    
    public void deleteUserFromGroupPerm(User user)       // 
    {
        Group group = this.groupOfID(user.gid);
        if(group != null)
        {
            group.groupUsers.remove(user);
        }
    }
    
    public void createGroup(String name)
    {
        if (!this.containsName(name))
        {
            Group newGroup = new Group(name, this.maxNumber);
            this.addGroup(newGroup);
            this.maxNumber++;
            System.out.println("[.] Group named " + newGroup.name + " with gid " + newGroup.gid + " was succesfully created.");
        }
        else
        {
            System.out.println("[!] The name of the group already exists.");
        }
    }
    
    public void createGroupOfID(String name, int gid)
    {
        if (!this.containsName(name))
        {
            Group newGroup = new Group(name, gid);
            this.addGroup(newGroup);
            //this.maxNumber++;
        }
        else
        {
            System.out.println("[!] The name of the group already exists.");
        }
    }
    
    public void deleteGroup(String name)
    {
        Group delete = this.groupOfName(name);
        if(delete != null)
        {
            for (User u: delete.groupUsers)
            {
                u.gid = 0;
                this.addUserToDefaultGroup(u);
            }
            this.list.remove(delete);
            System.out.println("\nThe group named " + delete.name + " was deleted");
        }
        else
        {
            System.out.println("\nNo group named " + name + " was found");
        }
    }
    
    public void printGroupProperties(Group g)
    {
        System.out.println("Name: " + g.name);
        System.out.println("GID: " + g.gid);
        System.out.println("Number of members: " + g.groupUsers.size());
        System.out.println();
    }
    
    public void printGroupProperties(Group g, String first)
    {
        System.out.println(first + "Name: " + g.name);
        System.out.println(first + "GID: " + g.gid);
        System.out.println(first + "Number of members: " + g.groupUsers.size());
        System.out.println();
    }
    
    public void printAllGroups()
    {
        System.out.println("[.] Printing all groups with properties:\n\n");
        for (Group g: this.list)
        {
            this.printGroupProperties(g, "\t");
        }
    }
    
    public void printAllMembersOfGroup(Group g, String first)
    {
        for (User u: g.groupUsers)
        {
            System.out.println(first + "Name: " + u.name);
            System.out.println(first + "ID: " + u.id);
            System.out.println(first + "GID: " + u.gid);
            System.out.println();
        }
    }
    
    public void printAllGroupsAndMembers()
    {
        System.out.print("[.] Printing all groups and their members:\n\n");
        for (Group g: this.list)
        {
            this.printGroupProperties(g, "\t");
            this.printAllMembersOfGroup(g, "\t\t");
        }
    }
    
    public Group groupOfID(int gid)
    {
        for(Group g: this.list)
        {
            if(g.gid == gid)
            {
                return g;
            }
        }
        return null;
    }
    
    public Group groupOfName(String name)
    {
        for(Group g: this.list)
        {
            if(g.name.equals(name))
            {
                return g;
            }
        }
        return null;
    }
    
    public boolean hasGroupOfID(int gid)
    {
        Group temp = this.groupOfID(gid);
        if (temp != null)
        {
            return true;
        }
        return false;
    }
    
    public boolean hasGroupOfName(String name)
    {
        Group temp = this.groupOfName(name);
        if (temp != null)
        {
            return true;
        }
        return false;
    }
    
    public boolean containsName(String name)
    {
        for(Group g: this.list)
        {
            if(g.name.equals(name))
            {
                return true;
            }
        }
        return false;
    }
   
    public boolean hasBasicGroups()
    {
        if(this.list.isEmpty())
        {
            return false;
        }
        return true;
    }
    
    public void createBasicGroups()
    {
        this.createGroup("Admins");
        Group temp = this.groupOfName("Admins");
        System.out.println("[.] Creating " + temp.name + " group with gid == " + temp.gid + ", a basic group of administrators.");
        this.createGroup("Users");
        Group temp2 = this.groupOfName("Users");
        System.out.println("[.] Creating " + temp2.name + " group with gid == " + temp2.gid + ", a basic and default group of users.");
    }
    
    GroupList()
    {
        this.list = new ArrayList<>();
        this.maxNumber = 0;
    }
}